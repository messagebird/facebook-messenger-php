<?php

namespace MessageBird\FacebookMessenger\Objects;

use MessageBird\FacebookMessenger\Exceptions\InvalidArgumentException;

/**
 * Class Element
 *
 * @package MessageBird\FacebookMessenger\Objects
 */
class Element
{
    /**
     * @var Button[]
     */
    protected $buttons;

    /**
     * @var string
     */
    protected $title;

    /**
     * @var string
     */
    protected $itemUrl;

    /**
     * @var string
     */
    protected $imageUrl;

    /**
     * @var string
     */
    protected $subtitle;

    /**
     * Element constructor.
     *
     * @param string        $title
     * @param string|null   $itemUrl
     * @param string|null   $imageUrl
     * @param string|null   $subtitle
     * @param Button[]|null $buttons
     */
    public function __construct($title, $itemUrl = null, $imageUrl = null, $subtitle = null, array $buttons = [])
    {
        foreach ($buttons as $button) {
            if (!$button instanceof Button) {
                throw new InvalidArgumentException('Buttons array elements must be Button objects.');
            }
        }

        $this->title = $title;
        $this->itemUrl = $itemUrl;
        $this->imageUrl = $imageUrl;
        $this->subtitle = $subtitle;
        $this->buttons = $buttons;
    }

    /**
     * @return Button[]
     */
    public function getButtons()
    {
        return $this->buttons;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @return string
     */
    public function getItemUrl()
    {
        return $this->itemUrl;
    }

    /**
     * @return string
     */
    public function getImageUrl()
    {
        return $this->imageUrl;
    }

    /**
     * @return string
     */
    public function getSubtitle()
    {
        return $this->subtitle;
    }

    /**
     * @return array
     */
    public function formatAsArray()
    {
        $buttons = [];
        foreach ($this->buttons as $button) {
            $buttons[] = $button->formatAsArray();
        }

        return [
            'title' => $this->title,
            'item_url' => $this->itemUrl,
            'image_url' => $this->imageUrl,
            'subtitle' => $this->subtitle,
            'buttons' => $buttons ? $buttons : null,
        ];
    }
}
