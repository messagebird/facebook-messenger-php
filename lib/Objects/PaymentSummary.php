<?php

namespace MessageBird\FacebookMessenger\Objects;

/**
 * Class PaymentSummary
 *
 * @package MessageBird\FacebookMessenger\Objects
 */
class PaymentSummary
{
    /**
     * @var string
     */
    protected $totalCost;
    /**
     * @var string|null
     */
    protected $subTotal;
    /**
     * @var string|null
     */
    protected $shippingCost;
    /**
     * @var string|null
     */
    protected $totalTax;

    /**
     * PaymentSummary constructor.
     *
     * @param string      $totalCost
     * @param string|null $subTotal
     * @param string|null $shippingCost
     * @param string|null $totalTax
     */
    public function __construct($totalCost, $subTotal = null, $shippingCost = null, $totalTax = null)
    {
        $this->totalCost = $totalCost;
        $this->subTotal = $subTotal ? $subTotal : null;
        $this->shippingCost = $shippingCost ? $shippingCost : null;
        $this->totalTax = $totalTax ? $totalTax : null;
    }

    /**
     * @return string
     */
    public function getTotalCost()
    {
        return $this->totalCost;
    }

    /**
     * @return string|null
     */
    public function getSubTotal()
    {
        return $this->subTotal;
    }

    /**
     * @return string|null
     */
    public function getShippingCost()
    {
        return $this->shippingCost;
    }

    /**
     * @return string|null
     */
    public function getTotalTax()
    {
        return $this->totalTax;
    }

    /**
     * @return array
     */
    public function formatAsArray()
    {
        return [
            'subtotal' => $this->subTotal,
            'shipping_cost' => $this->shippingCost,
            'total_tax' => $this->totalTax,
            'total_cost' => $this->totalCost,
        ];
    }
}
