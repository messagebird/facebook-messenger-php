<?php

namespace MessageBird\FacebookMessenger\Objects;

/**
 * Interface MessageInterface
 *
 * @package MessageBird\FacebookMessenger\Objects
 */
interface MessageInterface
{
    /**
     * Returns an array representation of the message.
     *
     * @return array
     */
    public function formatAsArray();
}
