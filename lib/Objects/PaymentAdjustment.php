<?php

namespace MessageBird\FacebookMessenger\Objects;

/**
 * Class PaymentAdjustment
 *
 * @package MessageBird\FacebookMessenger\Objects
 */
class PaymentAdjustment
{
    /**
     * @var string|null
     */
    protected $name;
    /**
     * @var string| null
     */
    protected $amount;

    /**
     * PaymentAdjustment constructor.
     *
     * @param string|null $name
     * @param string|null $amount
     */
    public function __construct($name = null, $amount = null)
    {
        $this->name = $name;
        $this->amount = $amount;
    }

    /**
     * @return string|null
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return string|null
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * @return array
     */
    public function formatAsArray()
    {
        return [
            'name' => $this->name,
            'amount' => $this->amount,
        ];
    }
}
