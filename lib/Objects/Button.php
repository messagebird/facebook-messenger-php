<?php

namespace MessageBird\FacebookMessenger\Objects;

/**
 * Class Button
 *
 * @package MessageBird\FacebookMessenger\Objects
 */
class Button
{
    /**
     * @var string
     */
    protected $type;

    /**
     * @var string
     */
    protected $title;

    /**
     * @var string|null
     */
    protected $url;

    /**
     * @var string|null
     */
    protected $payload;

    /**
     * Button constructor.
     *
     * @param string      $type
     * @param string      $title
     * @param string|null $url
     * @param string|null $payload
     */
    public function __construct($type, $title, $url = null, $payload = null)
    {
        $this->type = $type;
        $this->title = $title;
        $this->url = $url;
        $this->payload = $payload;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @return string|null
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @return string|null
     */
    public function getPayload()
    {
        return $this->payload;
    }

    /**
     * @return array
     */
    public function formatAsArray()
    {
        return [
            'type' => $this->type,
            'title' => $this->title,
            'url' => $this->url,
            'payload' => $this->payload,
        ];
    }
}
