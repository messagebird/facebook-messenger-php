<?php

namespace MessageBird\FacebookMessenger\Objects;

use Psr\Http\Message\StreamInterface;

/**
 * Interface FileMessageInterface
 *
 * @package MessageBird\FacebookMessenger\Objects
 */
interface FileMessageInterface extends MessageInterface
{
    /**
     * Returns the name of the file.
     *
     * @return string
     */
    public function getFilename();

    /**
     * Returns the PHP stream related to the message.
     *
     * @return StreamInterface
     */
    public function getStream();

    /**
     * Returns the content type of the file.
     *
     * @return string
     */
    public function getContentType();
}
