<?php

namespace MessageBird\FacebookMessenger\Objects;

/**
 * Class AbstractReceivedMessage
 *
 * @package MessageBird\FacebookMessenger\Objects
 */
class AbstractReceivedMessage implements ReceivedMessageInterface
{
    /**
     * @var string
     */
    protected $mid;
    /**
     * @var int
     */
    protected $seq;

    /**
     * AbstractReceivedMessage constructor.
     *
     * @param string $mid
     * @param string $seq
     */
    public function __construct($mid, $seq)
    {
        $this->mid = $mid;
        $this->seq = $seq;
    }

    /**
     * @return string
     */
    public function getMid()
    {
        return $this->mid;
    }

    /**
     * @return int
     */
    public function getSequenceNumber()
    {
        return $this->seq;
    }
}
