<?php

namespace MessageBird\FacebookMessenger\Objects;

/**
 * Class CallbackUnsupported
 *
 * @package MessageBird\FacebookMessenger\Objects
 */
class CallbackUnsupported
{
    /**
     * @var array
     */
    protected $contents;

    /**
     * CallbackReceivedMessage constructor.
     *
     * @param array $contents
     */
    public function __construct(array $contents)
    {
            $this->contents = $contents;
    }

    /**
     * @return array
     */
    public function getContents()
    {
        return $this->contents;
    }

    /**
     * @inheritdoc
     */
    public function getType()
    {
        return 'callback_unsupported';
    }
}
