<?php

namespace MessageBird\FacebookMessenger\Objects;

/**
 * Class ReceivedTextMessage
 *
 * @package MessageBird\FacebookMessenger\Objects
 */
class ReceivedTextMessage extends AbstractReceivedMessage
{
    /**
     * @var string
     */
    protected $text;

    /**
     * ReceivedTextMessage constructor.
     *
     * @param string $mid
     * @param string $seq
     * @param string $text
     */
    public function __construct($mid, $seq, $text)
    {
        parent::__construct($mid, $seq);
        $this->text = $text;
    }

    /**
     * @return string
     */
    public function getText()
    {
        return $this->text;
    }
}
