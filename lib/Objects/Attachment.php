<?php

namespace MessageBird\FacebookMessenger\Objects;

/**
 * Class Attachment
 *
 * @package MessageBird\FacebookMessenger\Objects
 */
class Attachment
{
    /**
     * @var string
     */
    protected $title;
    /**
     * @var string
     */
    protected $url;
    /**
     * @var string
     */
    protected $type;
    /**
     * @var string
     */
    protected $payload;

    /**
     * Attachment constructor.
     *
     * @param string $type
     * @param string $payload
     * @param null   $title
     * @param null   $url
     */
    public function __construct($type, $payload, $title = null, $url = null)
    {
        $this->type = $type;
        $this->payload = $payload;
        $this->title = $title;
        $this->url = $url;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @return string
     */
    public function getPayload()
    {
        return $this->payload;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }
}
