<?php

namespace MessageBird\FacebookMessenger\Objects;

use MessageBird\FacebookMessenger\Exceptions\InvalidArgumentException;

/**
 * Class GenericTemplate
 *
 * @package MessageBird\FacebookMessenger\Objects
 */
class GenericTemplate implements MessageInterface
{
    /**
     * @var Element[]
     */
    protected $elements;

    /**
     * GenericTemplate constructor.
     *
     * @param Element[] $elements
     */
    public function __construct(array $elements)
    {
        if (empty($elements)) {
            throw new InvalidArgumentException('Elements array is required.');
        }

        foreach ($elements as $element) {
            if (!$element instanceof Element) {
                throw new InvalidArgumentException('Elements array elements must be Element objects.');
            }
        }

        $this->elements = $elements;
    }

    /**
     * @return Element[]
     */
    public function getElements()
    {
        return $this->elements;
    }

    /**
     * @inheritdoc
     */
    public function formatAsArray()
    {
        $elements = [];
        foreach ($this->elements as $element) {
            $elements[] = $element->formatAsArray();
        }

        return [
            'attachment' => [
                'type' => 'template',
                'payload' => [
                    'template_type' => 'generic',
                    'elements' => $elements,
                ],
            ],
        ];
    }
}
