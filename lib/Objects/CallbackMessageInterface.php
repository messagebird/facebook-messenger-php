<?php

namespace MessageBird\FacebookMessenger\Objects;

/**
 * Interface CallbackMessageInterface
 *
 * @package MessageBird\FacebookMessenger\Objects
 */
interface CallbackMessageInterface
{
    /**
     * Returns the type of the callback message.
     *
     * @return string
     */
    public function getType();
}
