<?php

namespace MessageBird\FacebookMessenger\Objects;

/**
 * Class ReceiptItem
 *
 * @package MessageBird\FacebookMessenger\Objects
 */
class ReceiptItem
{
    /**
     * @var string
     */
    protected $title;
    /**
     * @var string|null
     */
    protected $subtitle;
    /**
     * @var int|null
     */
    protected $quantity;
    /**
     * @var string|null
     */
    protected $price;
    /**
     * @var string|null
     */
    protected $currency;
    /**
     * @var string|null
     */
    protected $imageUrl;

    /**
     * ReceiptItem constructor.
     *
     * @param string        $title
     * @param string|null   $subtitle
     * @param int|null      $quantity
     * @param string|null    $price
     * @param string|string $currency
     * @param string|null   $imageUrl
     */
    public function __construct(
        $title,
        $subtitle = null,
        $quantity = null,
        $price = null,
        $currency = null,
        $imageUrl = null
    ) {
        $this->title = $title;
        $this->subtitle = $subtitle;
        $this->quantity = $quantity;
        $this->price = $price;
        $this->currency = $currency;
        $this->imageUrl = $imageUrl;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @return string|null
     */
    public function getSubtitle()
    {
        return $this->subtitle;
    }

    /**
     * @return int|null
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * @return string|null
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @return string|null
     */
    public function getCurrency()
    {
        return $this->currency;
    }

    /**
     * @return string|null
     */
    public function getImageUrl()
    {
        return $this->imageUrl;
    }

    /**
     * @return array
     */
    public function formatAsArray()
    {
        return [
            'title' => $this->title,
            'subtitle' => $this->subtitle,
            'quantity' => $this->quantity,
            'price' => $this->price,
            'currency' => $this->currency,
            'image_url' => $this->imageUrl,
        ];
    }
}
