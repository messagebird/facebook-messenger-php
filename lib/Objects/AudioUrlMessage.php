<?php

namespace MessageBird\FacebookMessenger\Objects;

/**
 * Class AudioUrlMessage
 *
 * @package MessageBird\FacebookMessenger\Objects
 */
class AudioUrlMessage implements MessageInterface
{
    /**
     * @var string
     */
    protected $url;

    /**
     * AudioUrlMessage constructor.
     *
     * @param string $url
     */
    public function __construct($url)
    {
        $this->url = $url;
    }

    /**
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @inheritdoc
     */
    public function formatAsArray()
    {
        return [
            'attachment' => [
                'type' => 'audio',
                'payload' => [
                    'url' => $this->url,
                ],
            ],
        ];
    }
}
