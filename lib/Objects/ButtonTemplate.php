<?php

namespace MessageBird\FacebookMessenger\Objects;

use MessageBird\FacebookMessenger\Exceptions\InvalidArgumentException;

/**
 * Class ButtonTemplate
 *
 * @package MessageBird\FacebookMessenger\Objects
 */
class ButtonTemplate implements MessageInterface
{
    /**
     * @var string
     */
    protected $text;

    /**
     * @var Button[]
     */
    protected $buttons;

    /**
     * ButtonTemplate constructor.
     *
     * @param string   $text
     * @param Button[] $buttons
     *
     * @throws InvalidArgumentException
     */
    public function __construct($text, array $buttons)
    {
        if (empty($buttons)) {
            throw new InvalidArgumentException('Buttons array is required.');
        }

        foreach ($buttons as $button) {
            if (!$button instanceof Button) {
                throw new InvalidArgumentException('Buttons array elements must be Button objects.');
            }
        }

        $this->text = $text;
        $this->buttons = $buttons;
    }

    /**
     * @return string
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * @return Button[]
     */
    public function getButtons()
    {
        return $this->buttons;
    }

    /**
     * @inheritdoc
     */
    public function formatAsArray()
    {
        $buttons = [];
        foreach ($this->buttons as $button) {
            $buttons[] = $button->formatAsArray();
        }

        return [
            'attachment' => [
                'type' => 'template',
                'payload' => [
                    'template_type' => 'button',
                    'text' => $this->text,
                    'buttons' => $buttons,
                ],
            ],
        ];
    }
}
