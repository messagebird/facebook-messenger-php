<?php

namespace MessageBird\FacebookMessenger\Objects;

use MessageBird\FacebookMessenger\Exceptions\InvalidArgumentException;

/**
 * Class ReceiptTemplate
 *
 * @package MessageBird\FacebookMessenger\Objects
 */
class ReceiptTemplate implements MessageInterface
{
    /**
     * @var string
     */
    protected $recipientName;
    /**
     * @var string
     */
    protected $orderNumber;
    /**
     * @var string
     */
    protected $currency;
    /**
     * @var string
     */
    protected $paymentMethod;
    /**
     * @var ReceiptItem[]
     */
    protected $receiptItems;
    /**
     * @var PaymentSummary
     */
    protected $paymentSummary;
    /**
     * @var string|null
     */
    protected $timestamp;
    /**
     * @var string|null
     */
    protected $orderUrl;
    /**
     * @var Address|null
     */
    protected $shippingAddress;
    /**
     * @var PaymentAdjustment[]|null
     */
    protected $paymentAdjustments;

    /**
     * ReceiptTemplate constructor.
     *
     * @param string                   $recipientName
     * @param string                   $orderNumber
     * @param string                   $currency
     * @param string                   $paymentMethod
     * @param ReceiptItem[]            $receiptItems
     * @param PaymentSummary           $paymentSummary
     * @param string|null              $timestamp
     * @param string|null              $orderUrl
     * @param Address|null             $shippingAddress
     * @param PaymentAdjustment[]|null $paymentAdjustments
     */
    public function __construct(
        $recipientName,
        $orderNumber,
        $currency,
        $paymentMethod,
        array $receiptItems,
        $paymentSummary,
        $timestamp = null,
        $orderUrl = null,
        Address $shippingAddress = null,
        array $paymentAdjustments = []
    ) {
        if (empty($receiptItems)) {
            throw new InvalidArgumentException('Receipt items array is required.');
        }

        foreach ($receiptItems as $receiptItem) {
            if (!$receiptItem instanceof ReceiptItem) {
                throw new InvalidArgumentException('Receipt items array elements must be ReceiptItem objects.');
            }
        }

        foreach ($paymentAdjustments as $paymentAdjustment) {
            if (!$paymentAdjustment instanceof PaymentAdjustment) {
                throw new InvalidArgumentException(
                    'Payment adjustments array elements must be PaymentAdjustment objects.'
                );
            }
        }

        $this->recipientName = $recipientName;
        $this->orderNumber = $orderNumber;
        $this->currency = $currency;
        $this->paymentMethod = $paymentMethod;
        $this->receiptItems = $receiptItems;
        $this->paymentSummary = $paymentSummary;
        $this->timestamp = $timestamp;
        $this->orderUrl = $orderUrl;
        $this->shippingAddress = $shippingAddress;
        $this->paymentAdjustments = $paymentAdjustments;
    }

    /**
     * @return string
     */
    public function getRecipientName()
    {
        return $this->recipientName;
    }

    /**
     * @return string
     */
    public function getOrderNumber()
    {
        return $this->orderNumber;
    }

    /**
     * @return string
     */
    public function getCurrency()
    {
        return $this->currency;
    }

    /**
     * @return string
     */
    public function getPaymentMethod()
    {
        return $this->paymentMethod;
    }

    /**
     * @return ReceiptItem[]
     */
    public function getReceiptItems()
    {
        return $this->receiptItems;
    }

    /**
     * @return PaymentSummary
     */
    public function getPaymentSummary()
    {
        return $this->paymentSummary;
    }

    /**
     * @return null|string
     */
    public function getTimestamp()
    {
        return $this->timestamp;
    }

    /**
     * @return null|string
     */
    public function getOrderUrl()
    {
        return $this->orderUrl;
    }

    /**
     * @return Address|null
     */
    public function getShippingAddress()
    {
        return $this->shippingAddress;
    }

    /**
     * @return PaymentAdjustment[]|null
     */
    public function getPaymentAdjustments()
    {
        return $this->paymentAdjustments;
    }

    /**
     * @inheritdoc
     */
    public function formatAsArray()
    {
        $receiptItems = [];
        foreach ($this->receiptItems as $receiptItem) {
            $receiptItems[] = $receiptItem->formatAsArray();
        }

        $paymentAdjustments = [];
        foreach ($this->paymentAdjustments as $paymentAdjustment) {
            $paymentAdjustments[] = $paymentAdjustment->formatAsArray();
        }

        return [
            'attachment' => [
                'type' => 'template',
                'payload' => [
                    'template_type' => 'receipt',
                    'recipient_name' => $this->recipientName,
                    'order_number' => $this->orderNumber,
                    'currency' => $this->currency,
                    'payment_method' => $this->paymentMethod,
                    'timestamp' => $this->timestamp,
                    'order_url' => $this->orderUrl,
                    'elements' => $receiptItems,
                    'address' => $this->shippingAddress ? $this->shippingAddress->formatAsArray() : null,
                    'summary' => $this->paymentSummary->formatAsArray(),
                    'adjustments' => $paymentAdjustments ? $paymentAdjustments : null,
                ],
            ],
        ];
    }
}
