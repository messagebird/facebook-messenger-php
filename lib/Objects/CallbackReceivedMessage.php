<?php

namespace MessageBird\FacebookMessenger\Objects;

use MessageBird\FacebookMessenger\Exceptions\InvalidArgumentException;

/**
 * Class CallbackReceivedMessage
 *
 * @package MessageBird\FacebookMessenger\Objects
 */
class CallbackReceivedMessage extends AbstractCallbackMessage
{
    /**
     * @var int
     */
    protected $timestamp;
    /**
     * @var ReceivedMessageInterface
     */
    protected $message;

    /**
     * CallbackReceivedMessage constructor.
     *
     * @param array $message
     */
    public function __construct(array $message)
    {
        parent::__construct($message);

        if (empty($message['timestamp'])) {
            throw new InvalidArgumentException('Timestamp is required.');
        }

        if (empty($message['message'])) {
            throw new InvalidArgumentException('Message is required.');
        }

        if (empty($message['message']['mid'])) {
            throw new InvalidArgumentException('Message mid is required.');
        }

        if (empty($message['message']['seq'])) {
            throw new InvalidArgumentException('Message seq is required.');
        }

        $this->timestamp = $message['timestamp'];

        if (!empty($message['message']['text'])) {
            $this->message = new ReceivedTextMessage(
                $message['message']['mid'],
                $message['message']['seq'],
                $message['message']['text']
            );
        }

        if (isset($message['message']['attachments'])) {
            if (!is_array($message['message']['attachments'])) {
                throw new InvalidArgumentException('Message attachments must be an array.');
            }
            $this->message = new ReceivedAttachmentMessage(
                $message['message']['mid'],
                $message['message']['seq'],
                $message['message']['attachments']
            );
        }

        if (!isset($message['message']['text']) && !isset($message['message']['attachments'])) {
            throw new InvalidArgumentException('Message text or attachment is required.');
        }
    }

    /**
     * @return int
     */
    public function getTimestamp()
    {
        return $this->timestamp;
    }

    /**
     * @return ReceivedMessageInterface
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * @inheritdoc
     */
    public function getType()
    {
        return 'received_message';
    }
}
