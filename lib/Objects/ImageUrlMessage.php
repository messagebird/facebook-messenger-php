<?php

namespace MessageBird\FacebookMessenger\Objects;

/**
 * Class ImageUrlMessage
 *
 * @package MessageBird\FacebookMessenger\Objects
 */
class ImageUrlMessage implements MessageInterface
{
    /**
     * @var string
     */
    protected $url;

    /**
     * ImageUrlMessage constructor.
     *
     * @param string $url
     */
    public function __construct($url)
    {
        $this->url = $url;
    }

    /**
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @inheritdoc
     */
    public function formatAsArray()
    {
        return [
            'attachment' => [
                'type' => 'image',
                'payload' => [
                    'url' => $this->url,
                ],
            ],
        ];
    }
}
