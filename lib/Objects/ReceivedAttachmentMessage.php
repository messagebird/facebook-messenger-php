<?php

namespace MessageBird\FacebookMessenger\Objects;

use MessageBird\FacebookMessenger\Exceptions\InvalidArgumentException;

/**
 * Class ReceivedAttachmentMessage
 *
 * @package MessageBird\FacebookMessenger\Objects
 */
class ReceivedAttachmentMessage extends AbstractReceivedMessage
{
    /**
     * @var Attachment[]
     */
    protected $attachments;

    /**
     * ReceivedAttachmentMessage constructor.
     *
     * @param string $mid
     * @param string $seq
     * @param array  $attachments
     */
    public function __construct($mid, $seq, array $attachments)
    {
        parent::__construct($mid, $seq);

        if (empty($attachments)) {
            throw new InvalidArgumentException('Attachments is required.');
        }

        foreach ($attachments as $attachment) {
            $this->attachments[] = $this->createAttachment($attachment);
        }
    }

    /**
     * Returns an Attachment instance based off array data.
     *
     * @param array $data
     *
     * @return Attachment
     */
    protected function createAttachment(array $data)
    {
        if (empty($data['type'])) {
            throw new InvalidArgumentException('Attachment type is required.');
        }
        if (empty($data['payload'])) {
            throw new InvalidArgumentException('Attachment payload is required.');
        }

        $title = !empty($data['title']) ? $data['title'] : null;
        $url = !empty($data['url']) ? $data['url'] : null;

        return new Attachment($data['type'], $data['payload'], $title, $url);
    }

    /**
     * @return Attachment[]
     */
    public function getAttachments()
    {
        return $this->attachments;
    }
}
