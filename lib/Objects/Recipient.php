<?php

namespace MessageBird\FacebookMessenger\Objects;

use MessageBird\FacebookMessenger\Exceptions\BadFunctionCallException;

/**
 * Class Recipient
 *
 * @package MessageBird\FacebookMessenger\Objects
 */
class Recipient
{
    /**
     * @var string|null
     */
    protected $id;

    /**
     * @var string|null
     */
    protected $phoneNumber;

    /**
     * Recipient constructor.
     *
     * @param string|null $id
     * @param string|null $phoneNumber
     */
    public function __construct($id = null, $phoneNumber = null)
    {
        if (!empty($id)) {
            $this->id = $id;
        }

        if (!empty($phoneNumber)) {
            $this->phoneNumber = $phoneNumber;
        }

        if (empty($id) && empty($phoneNumber)) {
            throw new BadFunctionCallException('Id or phone number are required.');
        }
    }

    /**
     * @return string|null
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string|null
     */
    public function getPhoneNumber()
    {
        return $this->phoneNumber;
    }

    /**
     * Returns an array representation of the recipient.
     *
     * @return array
     */
    public function formatAsArray()
    {
        if (!empty($this->id)) {
            return [
                'id' => $this->id,
            ];
        } else {
            return [
                'phone_number' => $this->phoneNumber,
            ];
        }
    }
}
