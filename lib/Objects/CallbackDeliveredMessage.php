<?php

namespace MessageBird\FacebookMessenger\Objects;

use MessageBird\FacebookMessenger\Exceptions\InvalidArgumentException;

/**
 * Class CallbackDeliveredMessage
 *
 * @package MessageBird\FacebookMessenger\Objects
 */
class CallbackDeliveredMessage extends AbstractCallbackMessage
{
    /**
     * @var array
     */
    protected $mids;
    /**
     * @var string
     */
    protected $watermark;
    /**
     * @var int
     */
    protected $seq;

    /**
     * CallbackDeliveredMessage constructor.
     *
     * @param array $message
     */
    public function __construct(array $message)
    {
        parent::__construct($message);

        if (empty($message['delivery']['watermark'])) {
            throw new InvalidArgumentException('Watermark is required.');
        }

        if (empty($message['delivery']['seq'])) {
            throw new InvalidArgumentException('Seq is required.');
        }

        $this->mids = !empty($message['delivery']['mids']) ? $message['delivery']['mids'] : [];
        $this->watermark = $message['delivery']['watermark'];
        $this->seq = $message['delivery']['seq'];
    }

    /**
     * @return array
     */
    public function getMids()
    {
        return $this->mids;
    }

    /**
     * @return string
     */
    public function getWatermark()
    {
        return $this->watermark;
    }

    /**
     * @return int
     */
    public function getSequenceNumber()
    {
        return $this->seq;
    }

    /**
     * @inheritdoc
     */
    public function getType()
    {
        return 'delivered_message';
    }
}
