<?php

namespace MessageBird\FacebookMessenger\Objects;

/**
 * Class Address
 *
 * @package MessageBird\FacebookMessenger\Objects
 */
class Address
{
    /**
     * @var string
     */
    protected $street1;
    /**
     * @var string
     */
    protected $city;
    /**
     * @var string
     */
    protected $postalCode;
    /**
     * @var string
     */
    protected $state;
    /**
     * @var string
     */
    protected $country;
    /**
     * @var string|null
     */
    protected $street2;

    /**
     * Address constructor.
     *
     * @param string      $street1
     * @param string      $city
     * @param string      $postalCode
     * @param string      $state
     * @param string      $country
     * @param string|null $street2
     */
    public function __construct($street1, $city, $postalCode, $state, $country, $street2 = null)
    {
        $this->street1 = $street1;
        $this->city = $city;
        $this->postalCode = $postalCode;
        $this->state = $state;
        $this->country = $country;
        $this->street2 = $street2;
    }

    /**
     * @return string
     */
    public function getStreet1()
    {
        return $this->street1;
    }

    /**
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @return string
     */
    public function getPostalCode()
    {
        return $this->postalCode;
    }

    /**
     * @return string
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * @return string
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * @return string|null
     */
    public function getStreet2()
    {
        return $this->street2;
    }

    /**
     * @return array
     */
    public function formatAsArray()
    {
        return [
            'street_1' => $this->street1,
            'street_2' => $this->street2,
            'city' => $this->city,
            'postal_code' => $this->postalCode,
            'state' => $this->state,
            'country' => $this->country,
        ];
    }
}
