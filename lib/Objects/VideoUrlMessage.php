<?php

namespace MessageBird\FacebookMessenger\Objects;

/**
 * Class VideoUrlMessage
 *
 * @package MessageBird\FacebookMessenger\Objects
 */
class VideoUrlMessage implements MessageInterface
{
    /**
     * @var string
     */
    protected $url;

    /**
     * VideoUrlMessage constructor.
     *
     * @param string $url
     */
    public function __construct($url)
    {
        $this->url = $url;
    }

    /**
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @inheritdoc
     */
    public function formatAsArray()
    {
        return [
            'attachment' => [
                'type' => 'video',
                'payload' => [
                    'url' => $this->url,
                ],
            ],
        ];
    }
}
