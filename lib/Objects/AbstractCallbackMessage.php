<?php

namespace MessageBird\FacebookMessenger\Objects;

use MessageBird\FacebookMessenger\Exceptions\InvalidArgumentException;

/**
 * Class AbstractCallbackMessage
 *
 * @package MessageBird\FacebookMessenger\Objects
 */
abstract class AbstractCallbackMessage implements CallbackMessageInterface
{
    /**
     * @var int The sender user id.
     */
    protected $senderId;
    /**
     * @var int The recipient page id.
     */
    protected $recipientId;

    /**
     * CallbackMessage constructor.
     *
     * @param array $message
     */
    public function __construct(array $message)
    {
        if (empty($message['sender']['id'])) {
            throw new InvalidArgumentException('Sender ID is required.');
        }

        if (empty($message['recipient']['id'])) {
            throw new InvalidArgumentException('Recipient ID is required.');
        }

        $this->senderId = $message['sender']['id'];
        $this->recipientId = $message['recipient']['id'];
    }

    /**
     * @return int
     */
    public function getSenderId()
    {
        return $this->senderId;
    }

    /**
     * @return int
     */
    public function getRecipientId()
    {
        return $this->recipientId;
    }
}
