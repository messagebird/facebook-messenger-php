<?php

namespace MessageBird\FacebookMessenger\Objects;

use MessageBird\FacebookMessenger\Exceptions\InvalidArgumentException;

/**
 * Class CallbackAuthenticationMessage
 *
 * @package MessageBird\FacebookMessenger\Objects
 */
class CallbackAuthenticationMessage extends AbstractCallbackMessage
{
    /**
     * @var int
     */
    protected $timestamp;
    /**
     * @var string
     */
    protected $optInReference;

    /**
     * CallbackAuthenticationMessage constructor.
     *
     * @param array $message
     */
    public function __construct(array $message)
    {
        parent::__construct($message);

        if (empty($message['timestamp'])) {
            throw new InvalidArgumentException('Timestamp is required.');
        }

        if (empty($message['optin']['ref'])) {
            throw new InvalidArgumentException('Opt-in reference is required.');
        }

        $this->timestamp = $message['timestamp'];
        $this->optInReference = $message['optin']['ref'];
    }

    /**
     * @return int
     */
    public function getTimestamp()
    {
        return $this->timestamp;
    }

    /**
     * @return string
     */
    public function getOptInReference()
    {
        return $this->optInReference;
    }

    /**
     * @inheritdoc
     */
    public function getType()
    {
        return 'authentication';
    }
}
