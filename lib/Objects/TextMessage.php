<?php

namespace MessageBird\FacebookMessenger\Objects;

/**
 * Class TextMessage
 *
 * @package MessageBird\FacebookMessenger\Objects
 */
class TextMessage implements MessageInterface
{
    /**
     * @var string
     */
    protected $text;

    /**
     * TextMessage constructor.
     *
     * @param string $text
     */
    public function __construct($text)
    {
        $this->text = $text;
    }

    /**
     * @return string
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * @inheritdoc
     */
    public function formatAsArray()
    {
        return [
            'text' => $this->text,
        ];
    }
}
