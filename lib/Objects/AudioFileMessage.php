<?php

namespace MessageBird\FacebookMessenger\Objects;

use GuzzleHttp\Psr7\Stream;

/**
 * Class AudioFileMessage
 *
 * @package MessageBird\FacebookMessenger\Objects
 */
class AudioFileMessage implements FileMessageInterface
{
    /**
     * @var string
     */
    protected $filename;
    /**
     * @var Stream
     */
    protected $stream;
    /**
     * @var string
     */
    protected $contentType;

    /**
     * AudioFileMessage constructor.
     *
     * @param string      $filePath
     * @param string      $filename
     * @param string|null $contentType
     */
    public function __construct($filePath, $filename, $contentType = null)
    {
        $this->filename = $filename;
        $this->stream = new Stream(fopen($filePath, 'r'));
        $this->contentType = $contentType;
    }

    /**
     * @inheritdoc
     */
    public function getFilename()
    {
        return $this->filename;
    }

    /**
     * @inheritdoc
     */
    public function getStream()
    {
        return $this->stream;
    }

    /**
     * @inheritdoc
     */
    public function getContentType()
    {
        return $this->contentType;
    }

    /**
     * @inheritdoc
     */
    public function formatAsArray()
    {
        return [
            'attachment' => [
                'type' => 'audio',
                'payload' => [],
            ],
        ];
    }
}
