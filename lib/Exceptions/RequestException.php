<?php

namespace MessageBird\FacebookMessenger\Exceptions;

use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

/**
 * Class RequestException
 *
 * @package MessageBird\FacebookMessenger\Exceptions
 */
class RequestException extends \GuzzleHttp\Exception\RequestException implements FacebookMessengerException
{
    /**
     * Factory method to create a new exception with a normalized error message
     *
     * @param RequestInterface  $request  Request
     * @param ResponseInterface $response Response received
     * @param \Exception        $previous Previous exception
     * @param array             $ctx      Optional handler context.
     *
     * @return self
     */
    public static function create(
        RequestInterface $request,
        ResponseInterface $response = null,
        \Exception $previous = null,
        array $ctx = []
    ) {
        $e = parent::create($request, $response, $previous, $ctx);

        return new static($e->getMessage(), $e->getRequest(), $e->getResponse(), $previous, $ctx);
    }
}
