<?php

namespace MessageBird\FacebookMessenger\Exceptions;

/**
 * Class BadFunctionCallException
 *
 * @package MessageBird\FacebookMessenger\Exceptions
 */
class BadFunctionCallException extends \BadFunctionCallException implements FacebookMessengerException
{
}
