<?php

namespace MessageBird\FacebookMessenger\Exceptions;

/**
 * Class InvalidArgumentException
 *
 * @package MessageBird\FacebookMessenger\Exceptions
 */
class InvalidArgumentException extends \InvalidArgumentException implements FacebookMessengerException
{
}
