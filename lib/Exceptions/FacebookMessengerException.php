<?php

namespace MessageBird\FacebookMessenger\Exceptions;

/**
 * Interface FacebookMessengerException
 *
 * @package MessageBird\FacebookMessenger\Exceptions
 */
interface FacebookMessengerException
{
}
