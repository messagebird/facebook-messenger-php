<?php

namespace MessageBird\FacebookMessenger\Exceptions;

/**
 * Class DomainException
 *
 * @package MessageBird\FacebookMessenger\Exceptions
 */
class DomainException extends \DomainException implements FacebookMessengerException
{
}
