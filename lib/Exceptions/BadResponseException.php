<?php

namespace MessageBird\FacebookMessenger\Exceptions;

use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

/**
 * Class BadResponseException
 *
 * @package MessageBird\FacebookMessenger\Exceptions
 */
class BadResponseException extends \GuzzleHttp\Exception\BadResponseException implements FacebookMessengerException
{
    /**
     * @return string
     */
    public function getErrorMessage()
    {
        $body = json_decode($this->getResponse()->getBody()->getContents(), true);

        return $body && !empty($body['error']['message']) ? $body['error']['message'] : '';
    }

    /**
     * @return string
     */
    public function getErrorType()
    {
        $body = json_decode($this->getResponse()->getBody()->getContents(), true);

        return $body && !empty($body['error']['type']) ? $body['error']['type'] : '';
    }

    /**
     * @return string|int
     */
    public function getErrorCode()
    {
        $body = json_decode($this->getResponse()->getBody()->getContents(), true);

        return $body && !empty($body['error']['code']) ? $body['error']['code'] : 0;
    }

    /**
     * @return string
     */
    public function getErrorData()
    {
        $body = json_decode($this->getResponse()->getBody()->getContents(), true);

        return $body && !empty($body['error']['error_data']) ? $body['error']['error_data'] : '';
    }

    /**
     * @return string
     */
    public function getErrorFbTraceId()
    {
        $body = json_decode($this->getResponse()->getBody()->getContents(), true);

        return $body && !empty($body['error']['fbtrace_id']) ? $body['error']['fbtrace_id'] : '';
    }

    /**
     * Factory method to create a new exception with a normalized error message
     *
     * @param RequestInterface  $request  Request
     * @param ResponseInterface $response Response received
     * @param \Exception        $previous Previous exception
     * @param array             $ctx      Optional handler context.
     *
     * @return self
     */
    public static function create(
        RequestInterface $request,
        ResponseInterface $response = null,
        \Exception $previous = null,
        array $ctx = []
    ) {
        $e = parent::create($request, $response, $previous, $ctx);

        return new static($e->getMessage(), $e->getRequest(), $e->getResponse(), $previous, $ctx);
    }
}
