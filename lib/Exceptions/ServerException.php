<?php

namespace MessageBird\FacebookMessenger\Exceptions;

/**
 * Class ServerException
 *
 * @package MessageBird\FacebookMessenger\Exceptions
 */
class ServerException extends BadResponseException implements FacebookMessengerException
{
}
