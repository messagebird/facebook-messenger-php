<?php

namespace MessageBird\FacebookMessenger\Exceptions;

/**
 * Class ClientException
 *
 * @package MessageBird\FacebookMessenger\Exceptions
 */
class ClientException extends BadResponseException implements FacebookMessengerException
{
}
