<?php

namespace MessageBird\FacebookMessenger;

use League\Event\EventInterface;

/**
 * Class CallbackListener
 *
 * @package MessageBird\FacebookMessenger
 */
class CallbackListener extends \League\Event\CallbackListener
{
    /**
     * Handle an event, calling the callback without the Event object as the first argument.
     *
     * @param EventInterface $event
     */
    public function handle(EventInterface $event)
    {
        call_user_func_array($this->callback, array_slice(func_get_args(), 1));
    }
}
