<?php

namespace MessageBird\FacebookMessenger\Responses;

use MessageBird\FacebookMessenger\Exceptions\DomainException;
use Psr\Http\Message\ResponseInterface as PsrResponseInterface;

/**
 * Class UserProfileApiResponse
 *
 * @package MessageBird\FacebookMessenger\Responses
 */
class UserProfileApiResponse extends AbstractResponse
{
    /**
     * @var string
     */
    protected $firstName;
    /**
     * @var string
     */
    protected $lastName;
    /**
     * @var string
     */
    protected $profilePic;

    /**
     * UserProfileApiResponse constructor.
     *
     * @param PsrResponseInterface $response
     */
    public function __construct(PsrResponseInterface $response)
    {
        parent::__construct($response);
        $body = $this->formatBodyAsArray();
        if (empty($body['first_name'])) {
            throw new DomainException('First name is required.');
        }
        if (empty($body['last_name'])) {
            throw new DomainException('Last name is required.');
        }
        if (empty($body['profile_pic'])) {
            throw new DomainException('Profile pic is required.');
        }

        $this->firstName = $body['first_name'];
        $this->lastName = $body['last_name'];
        $this->profilePic = $body['profile_pic'];
    }

    /**
     * @return string
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * @return string
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * @return string
     */
    public function getProfilePictureUrl()
    {
        return $this->profilePic;
    }
}
