<?php

namespace MessageBird\FacebookMessenger\Responses;

/**
 * Interface ResponseInterface
 *
 * @package MessageBird\FacebookMessenger\Responses
 */
interface ResponseInterface
{
    public function formatBodyAsArray();
}
