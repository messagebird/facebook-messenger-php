<?php

namespace MessageBird\FacebookMessenger\Responses;

use MessageBird\FacebookMessenger\Exceptions\DomainException;
use Psr\Http\Message\ResponseInterface as PsrResponseInterface;

/**
 * Class ThreadSettingsResponse
 *
 * @package MessageBird\FacebookMessenger\Responses
 */
class ThreadSettingsResponse extends AbstractResponse
{
    /**
     * @var string
     */
    protected $result;

    /**
     * ThreadSettingsResponse constructor.
     *
     * @param PsrResponseInterface $response
     */
    public function __construct(PsrResponseInterface $response)
    {
        parent::__construct($response);
        $body = $this->formatBodyAsArray();
        if (empty($body['result'])) {
            throw new DomainException('Result is required.');
        }

        $this->result = $body['result'];
    }

    /**
     * @return string
     */
    public function getResult()
    {
        return $this->result;
    }
}
