<?php

namespace MessageBird\FacebookMessenger\Responses;

use MessageBird\FacebookMessenger\Exceptions\DomainException;
use Psr\Http\Message\ResponseInterface as PsrResponseInterface;

/**
 * Class AbstractResponse
 *
 * @package MessageBird\FacebookMessenger\Responses
 */
class AbstractResponse implements ResponseInterface
{
    /**
     * @var string
     */
    protected $body;

    /**
     * AbstractResponse constructor.
     *
     * @param PsrResponseInterface $response
     */
    public function __construct(PsrResponseInterface $response)
    {
        $this->body = $response->getBody()->getContents();
    }

    /**
     * @return string
     */
    public function getBody()
    {
        return $this->body;
    }

    /**
     * @return array
     */
    public function formatBodyAsArray()
    {
        $data = json_decode($this->body, true);

        if (!$data) {
            throw new DomainException('Body contains invalid JSON data.');
        }

        return $data;
    }
}
