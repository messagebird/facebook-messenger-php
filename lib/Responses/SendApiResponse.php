<?php

namespace MessageBird\FacebookMessenger\Responses;

use MessageBird\FacebookMessenger\Exceptions\DomainException;
use Psr\Http\Message\ResponseInterface as PsrResponseInterface;

/**
 * Class SendApiResponse
 *
 * @package MessageBird\FacebookMessenger\Responses
 */
class SendApiResponse extends AbstractResponse
{
    /**
     * @var string
     */
    protected $recipientId;
    /**
     * @var string
     */
    protected $messageId;

    /**
     * SendApiResponse constructor.
     *
     * @param PsrResponseInterface $response
     */
    public function __construct(PsrResponseInterface $response)
    {
        parent::__construct($response);
        $body = $this->formatBodyAsArray();
        if (empty($body['recipient_id'])) {
            throw new DomainException('Recipient ID is required.');
        }
        if (empty($body['message_id'])) {
            throw new DomainException('Message ID is required.');
        }

        $this->recipientId = $body['recipient_id'];
        $this->messageId = $body['message_id'];
    }

    /**
     * @return string
     */
    public function getRecipientId()
    {
        return $this->recipientId;
    }

    /**
     * @return string
     */
    public function getMessageId()
    {
        return $this->messageId;
    }
}
