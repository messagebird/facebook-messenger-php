<?php

namespace MessageBird\FacebookMessenger;

use GuzzleHttp\ClientInterface;
use League\Event\Emitter;
use League\Event\Event;
use MessageBird\FacebookMessenger\Exceptions\DomainException;
use MessageBird\FacebookMessenger\Objects\FileMessageInterface;
use MessageBird\FacebookMessenger\Objects\MessageInterface;
use MessageBird\FacebookMessenger\Objects\Recipient;
use MessageBird\FacebookMessenger\Requests\CallbackRequest;
use MessageBird\FacebookMessenger\Requests\SendApiRequest;
use MessageBird\FacebookMessenger\Requests\UserProfileApiRequest;
use MessageBird\FacebookMessenger\Requests\WelcomeMessageRequest;

/**
 * Class FacebookMessenger
 *
 * @package MessageBird\FacebookMessenger
 */
class FacebookMessenger
{
    /**
     * @var string
     */
    protected $accessToken;
    /**
     * @var Emitter;
     */
    protected $emitter;
    /**
     * @var string
     */
    protected $body;
    /**
     * @var string|null
     */
    protected $signature;
    /**
     * @var string|null
     */
    protected $appSecret;
    /**
     * @var string|null
     */
    protected $validationToken;
    /**
     * @var string|null
     */
    protected $verifyToken;
    /**
     * @var string|null
     */
    protected $challenge;

    /**
     * FacebookMessenger constructor.
     *
     * @param string|null          $accessToken
     * @param string|null          $appSecret
     * @param string|null          $validationToken
     * @param string|null          $verifyToken
     * @param string|null          $challenge
     * @param ClientInterface|null $client
     * @param Emitter|null         $emitter
     * @param string|null          $body
     * @param string|null          $signature
     */
    public function __construct(
        $accessToken = null,
        $appSecret = null,
        $validationToken = null,
        $verifyToken = null,
        $challenge = null,
        ClientInterface $client = null,
        Emitter $emitter = null,
        $body = null,
        $signature = null
    ) {
        $this->accessToken = $accessToken;
        $this->client = $client;
        $this->emitter = $emitter ?: new Emitter();
        $this->body = $body ?: file_get_contents('php://input');
        $this->signature = $signature ?: static::getSignatureFromHeader();
        $this->appSecret = $appSecret;
        $this->verifyToken = $verifyToken ?: static::getVerifyTokenFromQueryParams();
        $this->validationToken = $validationToken;
        $this->challenge = $challenge ?: static::getChallengeFromQueryParams();
    }

    /**
     * @return string
     */
    public function getAccessToken()
    {
        return $this->accessToken;
    }

    /**
     * @param string $accessToken
     */
    public function setAccessToken($accessToken)
    {
        $this->accessToken = $accessToken;
    }

    /**
     * Sends a message.
     *
     * @param Recipient        $recipient
     * @param MessageInterface $message
     *
     * @return Responses\SendApiResponse
     */
    public function sendMessage(Recipient $recipient, MessageInterface $message)
    {
        if (empty($this->accessToken)) {
            throw new DomainException('Access token is required.');
        }

        $request = new SendApiRequest($this->accessToken, $this->client);

        if ($message instanceof FileMessageInterface) {
            return $request->createFileMessage($recipient, $message);
        } else {
            return $request->createMessage($recipient, $message);
        }
    }

    /**
     * Gets a user profile.
     *
     * @param int $userId
     *
     * @return Responses\UserProfileApiResponse
     */
    public function getUserProfile($userId)
    {
        if (empty($this->accessToken)) {
            throw new DomainException('Access token is required.');
        }

        $request = new UserProfileApiRequest($this->accessToken, $this->client);

        return $request->get($userId);
    }

    /**
     * Sets a Welcome Message.
     *
     * @param int              $pageId
     * @param MessageInterface $message
     *
     * @return Responses\ThreadSettingsResponse
     */
    public function setWelcomeMessage($pageId, MessageInterface $message)
    {
        if (empty($this->accessToken)) {
            throw new DomainException('Access token is required.');
        }

        $request = new WelcomeMessageRequest($this->accessToken, $this->client);

        return $request->create($pageId, $message);
    }

    /**
     * Deletes the Welcome Message.
     *
     * @param                  $pageId
     *
     * @return Responses\ThreadSettingsResponse
     */
    public function deleteWelcomeMessage($pageId)
    {
        if (empty($this->accessToken)) {
            throw new DomainException('Access token is required.');
        }

        $request = new WelcomeMessageRequest($this->accessToken, $this->client);

        return $request->delete($pageId);
    }

    /**
     * Registers an event listener.
     *
     * @param string   $eventName
     * @param callable $callable
     */
    public function on($eventName, callable $callable)
    {
        $this->emitter->addListener($eventName, CallbackListener::fromCallable($callable));
    }

    /**
     * Handle incoming callback request.
     *
     * @return Event[]
     */
    public function handle()
    {
        $request = new CallbackRequest(
            $this->emitter,
            $this->appSecret,
            $this->body,
            $this->signature,
            $this->verifyToken,
            $this->validationToken,
            $this->challenge
        );

        return $request->handle();
    }

    /**
     * @return string|null
     */
    public static function getSignatureFromHeader()
    {
        if (empty($_SERVER['HTTP_X_HUB_SIGNATURE'])) {
            return null;
        } else {
            $header = $_SERVER['HTTP_X_HUB_SIGNATURE'];
        }

        if (preg_match('/^sha1=(.*)$/', $header, $matches)) {
            return $matches[1];
        } else {
            throw new DomainException('X-Hub-Signature header has invalid value.');
        }
    }

    /**
     * @return string|null
     */
    public static function getVerifyTokenFromQueryParams()
    {
        return empty($_GET['hub_verify_token']) ? null : $_GET['hub_verify_token'];
    }

    /**
     * @return string|null
     */
    public static function getChallengeFromQueryParams()
    {
        return empty($_GET['hub_challenge']) ? null : $_GET['hub_challenge'];
    }
}
