<?php

namespace MessageBird\FacebookMessenger\Requests;

use MessageBird\FacebookMessenger\Responses\UserProfileApiResponse;

/**
 * Class UserProfileApiRequest
 *
 * @package MessageBird\FacebookMessenger\Requests
 */
class UserProfileApiRequest extends AbstractRequest implements RequestInterface
{
    /**
     * @inheritdoc
     */
    public static function getResourceUri()
    {
        return 'https://graph.facebook.com/v2.6/';
    }

    /**
     * Gets a user profile.
     *
     * @param int $userId
     *
     * @return UserProfileApiResponse
     */
    public function get($userId)
    {
        $options = [
            'query' => 'access_token=' . $this->accessToken . '&fields=first_name,last_name,profile_pic',
        ];

        return new UserProfileApiResponse(parent::execute('GET', (string) $userId, $options));
    }
}
