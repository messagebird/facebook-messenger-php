<?php

namespace MessageBird\FacebookMessenger\Requests;

use League\Event\Emitter;
use League\Event\Event;
use MessageBird\FacebookMessenger\Exceptions\DomainException;
use MessageBird\FacebookMessenger\Exceptions\InvalidArgumentException;
use MessageBird\FacebookMessenger\Objects\CallbackAuthenticationMessage;
use MessageBird\FacebookMessenger\Objects\CallbackDeliveredMessage;
use MessageBird\FacebookMessenger\Objects\CallbackReceivedMessage;
use MessageBird\FacebookMessenger\Objects\CallbackUnsupported;

/**
 * Class CallbackRequest
 *
 * @package MessageBird\FacebookMessenger\Requests
 */
class CallbackRequest
{
    /**
     * @var string
     */
    const EVENT_AUTH_CALLBACK = 'authentication.callback';
    /**
     * @var string
     */
    const EVENT_MESSAGE_RECEIVED_CALLBACK = 'message.received.callback';
    /**
     * @var string
     */
    const EVENT_MESSAGE_DELIVERED_CALLBACK = 'message.delivered.callback';
    /**
     * @var string
     */
    const EVENT_WEBHOOK_VERIFICATION_CALLBACK = 'webhook.verification.callback';
    /**
     * @var string
     */
    const EVENT_CALLBACK_UNSUPPORTED =  'callback.unsupported';
    /**
     * @var Emitter
     */
    protected $emitter;
    /**
     * @var string
     */
    protected $body;
    /**
     * @var string|null
     */
    protected $verifyToken;
    /**
     * @var string|null
     */
    protected $validationToken;
    /**
     * @var string|null
     */
    protected $challenge;
    /**
     * @var string|null
     */
    protected $signature;
    /**
     * @var string|null
     */
    protected $rawBody;
    /**
     * @var string|null
     */
    protected $appSecret;

    /**
     * CallbackRequest constructor.
     *
     * @param Emitter     $emitter
     * @param string|null $appSecret
     * @param string|null $rawBody
     * @param string|null $signature
     * @param string|null $verifyToken
     * @param string|null $validationToken
     * @param string|null $challenge
     */
    public function __construct(
        Emitter $emitter,
        $appSecret = null,
        $rawBody = null,
        $signature = null,
        $verifyToken = null,
        $validationToken = null,
        $challenge = null
    ) {
        $this->body = json_decode($rawBody, true);
        if ($rawBody && !$this->body) {
            throw new InvalidArgumentException('Request body does not contain valid JSON.');
        }

        $this->emitter = $emitter;
        $this->verifyToken = $verifyToken;
        $this->validationToken = $validationToken;
        $this->challenge = $challenge;
        $this->signature = $signature;
        $this->rawBody = $rawBody;
        $this->appSecret = $appSecret;
    }

    /**
     * @param $signature
     * @param $data
     * @param $secret
     *
     * @return bool
     */
    public static function isValidSignature($signature, $data, $secret)
    {
        return $signature === hash_hmac('sha1', $data, $secret);
    }

    /**
     * Emit events based on incoming request data.
     *
     * @return Event[]
     */
    public function handle()
    {
        $events = [];

        if (!empty($this->body) && empty($this->body['entry'])) {
            throw new DomainException('Request body is missing the `entry` array.');
        }

        if (!empty($this->body) && !is_array($this->body['entry'])) {
            throw new DomainException('The `entry` element in the request body must be an array.');
        }

        if (!empty($this->body)) {
            if (!static::isValidSignature($this->signature, $this->rawBody, $this->appSecret)) {
                throw new DomainException('Signature verification failed.');
            }

            foreach ($this->body['entry'] as $entry) {
                foreach ($entry['messaging'] as $message) {
                    if (isset($message['optin'])) {
                        $events[] = $this->emitter->emit(
                            Event::named(self::EVENT_AUTH_CALLBACK),
                            new CallbackAuthenticationMessage($message),
                            $entry['id'],
                            $entry['time']
                        );
                    } elseif (isset($message['message']) &&
                        (empty($message['message']['is_echo']) || $message['message']['is_echo'] === false)) {
                        $events[] = $this->emitter->emit(
                            Event::named(self::EVENT_MESSAGE_RECEIVED_CALLBACK),
                            new CallbackReceivedMessage($message),
                            $entry['id'],
                            $entry['time']
                        );
                    } elseif (isset($message['delivery'])) {
                        $events[] = $this->emitter->emit(
                            Event::named(self::EVENT_MESSAGE_DELIVERED_CALLBACK),
                            new CallbackDeliveredMessage($message),
                            $entry['id'],
                            $entry['time']
                        );
                    } else {
                        $events[] = $this->emitter->emit(
                            Event::named(self::EVENT_CALLBACK_UNSUPPORTED),
                            new CallbackUnsupported($message),
                            $entry['id'],
                            $entry['time']
                        );
                    }
                }
            }
        }

        if (!empty($this->verifyToken && !empty($this->validationToken))) {
            $events[] = $this->emitter->emit(
                Event::named(self::EVENT_WEBHOOK_VERIFICATION_CALLBACK),
                self::isValidVerifyToken($this->verifyToken, $this->validationToken),
                $this->challenge
            );
        }

        return $events;
    }

    /**
     * @param string $verifyToken
     * @param string $validationToken
     *
     * @return bool
     */
    public static function isValidVerifyToken($verifyToken, $validationToken)
    {
        return $verifyToken === $validationToken;
    }
}
