<?php

namespace MessageBird\FacebookMessenger\Requests;

use MessageBird\FacebookMessenger\Objects\FileMessageInterface;
use MessageBird\FacebookMessenger\Objects\MessageInterface;
use MessageBird\FacebookMessenger\Objects\Recipient;
use MessageBird\FacebookMessenger\Responses\SendApiResponse;

/**
 * Class SendApiRequest
 *
 * @package MessageBird\FacebookMessenger\Requests
 */
class SendApiRequest extends AbstractRequest implements RequestInterface
{
    /**
     * @inheritdoc
     */
    public static function getResourceUri()
    {
        return 'https://graph.facebook.com/v2.6/me/messages/';
    }

    /**
     * Creates a message using the Send API.
     *
     * @param Recipient        $recipient
     * @param MessageInterface $message
     *
     * @return SendApiResponse
     */
    public function createMessage(Recipient $recipient, MessageInterface $message)
    {
        $options = [
            'json' => [
                'recipient' => $recipient->formatAsArray(),
                'message' => $message->formatAsArray(),
            ],
        ];

        return new SendApiResponse(parent::execute('POST', null, $options));
    }

    /**
     * Creates a message based on a message with file data.
     *
     * @param Recipient            $recipient
     * @param FileMessageInterface $message
     *
     * @return SendApiResponse
     */
    public function createFileMessage(Recipient $recipient, FileMessageInterface $message)
    {
        $options = [
            'multipart' => [
                [
                    'name' => 'recipient',
                    'contents' => json_encode($recipient->formatAsArray()),
                ],
                [
                    'name' => 'message',
                    'contents' => json_encode($message->formatAsArray()),
                ],
                [
                    'name' => 'filedata',
                    'contents' => $message->getStream(),
                    'filename' => $message->getFilename(),
                    'headers' => $message->getContentType() ? [
                        'Content-Type' => $message->getContentType(),
                    ] : [],
                ],
            ],
        ];

        return new SendApiResponse(parent::execute('POST', null, $options));
    }
}
