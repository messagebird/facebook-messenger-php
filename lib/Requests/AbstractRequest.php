<?php

namespace MessageBird\FacebookMessenger\Requests;

use GuzzleHttp\Client;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\BadResponseException as GuzzleBadResponseException;
use GuzzleHttp\Exception\ClientException as GuzzleClientException;
use GuzzleHttp\Exception\RequestException as GuzzleRequestException;
use GuzzleHttp\Exception\ServerException as GuzzleServerException;
use MessageBird\FacebookMessenger\Exceptions\BadResponseException;
use MessageBird\FacebookMessenger\Exceptions\ClientException;
use MessageBird\FacebookMessenger\Exceptions\RequestException;
use MessageBird\FacebookMessenger\Exceptions\ServerException;
use Psr\Http\Message\ResponseInterface;

/**
 * Class AbstractRequest
 *
 * @package MessageBird\FacebookMessenger\Requests
 */
abstract class AbstractRequest implements RequestInterface
{
    /**
     * @var ClientInterface
     */
    protected $client;

    /**
     * @var string $accesstoken
     */
    protected $accessToken;

    /**
     * AbstractRequest constructor.
     *
     * @param string               $accessToken
     * @param ClientInterface|null $client
     */
    public function __construct($accessToken, ClientInterface $client = null)
    {
        $this->accessToken = $accessToken;
        $this->client = $client ?: new Client(['base_uri' => static::getResourceUri()]);
    }

    /**
     * Perform the request.
     *
     * @param string $method
     * @param string $uri
     * @param array  $options
     *
     * @return ResponseInterface
     */
    public function execute($method, $uri = null, $options = [])
    {
        $options = array_merge(['query' => ['access_token' => $this->accessToken]], $options);

        try {
            return $this->client->request($method, $uri, $options);
        } catch (GuzzleClientException $e) {
            throw ClientException::create($e->getRequest(), $e->getResponse());
        } catch (GuzzleServerException $e) {
            throw ServerException::create($e->getRequest(), $e->getResponse());
        } catch (GuzzleBadResponseException $e) {
            throw BadResponseException::create($e->getRequest(), $e->getResponse());
        } catch (GuzzleRequestException $e) {
            throw RequestException::create($e->getRequest(), $e->getResponse());
        }
    }
}
