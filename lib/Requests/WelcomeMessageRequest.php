<?php

namespace MessageBird\FacebookMessenger\Requests;

use MessageBird\FacebookMessenger\Objects\MessageInterface;
use MessageBird\FacebookMessenger\Responses\AbstractResponse;
use MessageBird\FacebookMessenger\Responses\ThreadSettingsResponse;

/**
 * Class WelcomeMessageRequest
 *
 * @package MessageBird\FacebookMessenger\Requests
 */
class WelcomeMessageRequest extends AbstractRequest implements RequestInterface
{
    /**
     * @inheritdoc
     */
    public static function getResourceUri()
    {
        return 'https://graph.facebook.com/v2.6/';
    }

    /**
     * Sets a new Welcome Message for a page.
     *
     * @param int              $pageId
     * @param MessageInterface $message
     *
     * @return ThreadSettingsResponse
     */
    public function create($pageId, MessageInterface $message)
    {
        $options = [
            'json' => [
                'setting_type' => 'call_to_actions',
                'thread_state' => 'new_thread',
                'call_to_actions' => [
                    [
                        'message' => $message->formatAsArray(),
                    ],
                ],
            ],
        ];

        return new ThreadSettingsResponse(parent::execute('POST', "{$pageId}/thread_settings", $options));
    }

    /**
     * Deletes a Welcome Message for a page.
     *
     * @param string $pageId
     *
     * @return AbstractResponse
     */
    public function delete($pageId)
    {
        $options = [
            'json' => [
                'setting_type' => 'call_to_actions',
                'thread_state' => 'new_thread',
                'call_to_actions' => [],
            ],
        ];

        return new ThreadSettingsResponse(parent::execute('POST', "{$pageId}/thread_settings", $options));
    }
}
