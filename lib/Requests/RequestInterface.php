<?php

namespace MessageBird\FacebookMessenger\Requests;

/**
 * Interface RequestInterface
 *
 * @package MessageBird\FacebookMessenger\Requests
 */
interface RequestInterface
{
    /**
     * @return string The URL for accessing the request.
     */
    public static function getResourceUri();
}
