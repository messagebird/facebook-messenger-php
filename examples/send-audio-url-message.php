<?php

require __DIR__ . '/../vendor/autoload.php';

use MessageBird\FacebookMessenger\FacebookMessenger;
use MessageBird\FacebookMessenger\Objects\AudioUrlMessage;
use MessageBird\FacebookMessenger\Objects\Recipient;

// Initialize with a Facebook Page access token.
$messenger = new FacebookMessenger('PAGE_ACCESS_TOKEN');

// Recipient is a (page scoped) user ID in this example. Alternatively, `null` can be passed and a phone number can be
// used as a second constructor argument.
$recipient = new Recipient(850498881729033);

// Create message object.
$message = new AudioUrlMessage('https://archive.org/download/testmp3testfile/mpthreetest.mp3');

// Send message.
$response = $messenger->sendMessage($recipient, $message);

// Output returned message ID.
var_dump($response->getMessageId());
