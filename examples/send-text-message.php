<?php

require __DIR__ . '/../vendor/autoload.php';

use MessageBird\FacebookMessenger\FacebookMessenger;
use MessageBird\FacebookMessenger\Objects\Recipient;
use MessageBird\FacebookMessenger\Objects\TextMessage;

// Initialize with a Facebook Page access token.
$messenger = new FacebookMessenger('PAGE_ACCESS_TOKEN');

// Recipient is a (page scoped) user ID in this example. Alternatively, `null` can be passed and a phone number can be
// used as a second constructor argument.
$recipient = new Recipient(850498881729033);

// Create message object.
$message = new TextMessage('Hello, world!');

// Send message.
$response = $messenger->sendMessage($recipient, $message);

// Output returned message ID.
var_dump($response->getMessageId());
