<?php

require __DIR__ . '/../vendor/autoload.php';

use MessageBird\FacebookMessenger\FacebookMessenger;

// Initialize with a Facebook Page access token.
$messenger = new FacebookMessenger('PAGE_ACCESS_TOKEN');

// Delete the Welcome Message for a page.
$response = $messenger->deleteWelcomeMessage(248043208882480);

// Output request result.
var_dump($response->getResult());
