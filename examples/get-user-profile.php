<?php

require __DIR__ . '/../vendor/autoload.php';

use MessageBird\FacebookMessenger\FacebookMessenger;

// Initialize with a Facebook Page access token.
$messenger = new FacebookMessenger('PAGE_ACCESS_TOKEN');

// Get user profile for a page scoped user ID.
$response = $messenger->getUserProfile(850498881729033);

// Output returned user profile data.
var_dump($response->getFirstName(), $response->getLastName(), $response->getProfilePictureUrl());
