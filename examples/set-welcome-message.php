<?php

require __DIR__ . '/../vendor/autoload.php';

use MessageBird\FacebookMessenger\FacebookMessenger;
use MessageBird\FacebookMessenger\Objects\TextMessage;

// Initialize with a Facebook Page access token.
$messenger = new FacebookMessenger('PAGE_ACCESS_TOKEN');

// Create message object.
$message = new TextMessage('Welcome! How can we help you today?');

// Set the Welcome Message for a page.
$response = $messenger->setWelcomeMessage(248043208882480, $message);

// Output request result.
var_dump($response->getResult());
