<?php

require __DIR__ . '/../vendor/autoload.php';

use MessageBird\FacebookMessenger\FacebookMessenger;
use MessageBird\FacebookMessenger\Objects\ImageFileMessage;
use MessageBird\FacebookMessenger\Objects\Recipient;

// Initialize with a Facebook Page access token.
$messenger = new FacebookMessenger('PAGE_ACCESS_TOKEN');

// Recipient is a (page scoped) user ID in this example. Alternatively, `null` can be passed and a phone number can be
// used as a second constructor argument.
$recipient = new Recipient(850498881729033);

// Get sample image.
$filePath = tempnam(sys_get_temp_dir(), 'sample_images');
file_put_contents($filePath, fopen('https://placehold.it/100x100', 'r'));

// Create message object.
$message = new ImageFileMessage($filePath, 'image.png');

// Send message.
$response = $messenger->sendMessage($recipient, $message);

// Output returned message ID.
var_dump($response->getMessageId());
