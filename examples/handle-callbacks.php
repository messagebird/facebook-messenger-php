<?php

require __DIR__ . '/../vendor/autoload.php';

use MessageBird\FacebookMessenger\FacebookMessenger;
use MessageBird\FacebookMessenger\Objects\CallbackAuthenticationMessage;
use MessageBird\FacebookMessenger\Objects\CallbackDeliveredMessage;
use MessageBird\FacebookMessenger\Objects\CallbackReceivedMessage;

// Initialize with app secret (needed for signature validation) and validation token (needed for webhook verification).
$messenger = new FacebookMessenger(null, 'APP_SECRET', 'VALIDATION_TOKEN');

// Register event handler for webhook validation callbacks from the Facebook Messenger platform.
$messenger->on('webhook.verification.callback', function ($isVerified, $challenge) {
    /**
     * The HTTP response should have the challenge as the body.
     * @see https://developers.facebook.com/docs/messenger-platform/quickstart
     */
    if ($isVerified) {
        echo $challenge;
    }
});

// Register event handler for authentication callbacks from the Facebook Messenger platform.
$messenger->on('authentication.callback', function (CallbackAuthenticationMessage $message, $pageId, $timestamp) {
    var_dump(
        $message->getRecipientId(),
        $message->getSenderId(),
        $message->getOptInReference(),
        $message->getTimestamp(),
        $pageId,
        $timestamp
    );
});

// Register event handler for message received callbacks from the Facebook Messenger platform.
$messenger->on('message.received.callback', function (CallbackReceivedMessage $message, $pageId, $timestamp) {
    var_dump(
        $message->getRecipientId(),
        $message->getSenderId(),
        $message->getTimestamp(),
        $message->getMessage(),
        $pageId,
        $timestamp
    );
});

// Register event handler for message delivered callbacks from the Facebook Messenger platform.
$messenger->on('message.delivered.callback', function (CallbackDeliveredMessage $message, $pageId, $timestamp) {
    var_dump(
        $message->getRecipientId(),
        $message->getSenderId(),
        $message->getMids(),
        $message->getSequenceNumber(),
        $message->getWatermark(),
        $pageId,
        $timestamp
    );
});

// Handle incoming request.
$messenger->handle();
