<?php

require __DIR__ . '/../vendor/autoload.php';

use MessageBird\FacebookMessenger\FacebookMessenger;
use MessageBird\FacebookMessenger\Objects\AudioFileMessage;
use MessageBird\FacebookMessenger\Objects\Recipient;

// Initialize with a Facebook Page access token.
$messenger = new FacebookMessenger('PAGE_ACCESS_TOKEN');

// Recipient is a (page scoped) user ID in this example. Alternatively, `null` can be passed and a phone number can be
// used as a second constructor argument.
$recipient = new Recipient(850498881729033);

// Get sample audio.
$filePath = tempnam(sys_get_temp_dir(), 'sample_audio');
file_put_contents($filePath, fopen('https://archive.org/download/testmp3testfile/mpthreetest.mp3', 'r'));

// Create message object. Note: For MP3 audio file, Facebook requires the mime type to be `audio/mp3` instead of the
// more commonly used `audio/mpeg` (e.g. by httpd).
$message = new AudioFileMessage($filePath, 'mpthreetest.mp3', 'audio/mp3');

// Send message.
$response = $messenger->sendMessage($recipient, $message);

// Output returned message ID.
var_dump($response->getMessageId());
