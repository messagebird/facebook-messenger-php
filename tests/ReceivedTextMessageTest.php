<?php

use MessageBird\FacebookMessenger\Objects\ReceivedTextMessage;

class ReceivedTextMessageTest extends PHPUnit_Framework_TestCase
{
    public function testGetters()
    {
        $message = new ReceivedTextMessage('mid.1457764197618:41d102a3e1ae206a38', 73, 'foobar');
        $this->assertEquals('mid.1457764197618:41d102a3e1ae206a38', $message->getMid());
        $this->assertEquals(73, $message->getSequenceNumber());
        $this->assertEquals('foobar', $message->getText());
    }
}
