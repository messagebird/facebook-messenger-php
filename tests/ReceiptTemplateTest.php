<?php

use MessageBird\FacebookMessenger\Objects\Address;
use MessageBird\FacebookMessenger\Objects\PaymentAdjustment;
use MessageBird\FacebookMessenger\Objects\PaymentSummary;
use MessageBird\FacebookMessenger\Objects\ReceiptItem;
use MessageBird\FacebookMessenger\Objects\ReceiptTemplate;

class ReceiptTemplateTest extends PHPUnit_Framework_TestCase
{
    public function testEmptyReceiptItemsArray()
    {
        $this->setExpectedException(
            'MessageBird\FacebookMessenger\Exceptions\InvalidArgumentException',
            'Receipt items array is required.'
        );
        $paymentSummary = new PaymentSummary(42.50);
        $shippingAddress = new Address('Street 1', 'City', '90210', 'CA', 'USA');
        $paymentAdjustment = new PaymentAdjustment('foo');
        new ReceiptTemplate(
            'foo',
            'ABC1234',
            'EUR',
            'MasterCard xx1234',
            [],
            $paymentSummary,
            123456,
            'https://example.com',
            $shippingAddress,
            [$paymentAdjustment]
        );
    }

    public function testReceiptItemsArrayElements()
    {
        $this->setExpectedException(
            'MessageBird\FacebookMessenger\Exceptions\InvalidArgumentException',
            'Receipt items array elements must be ReceiptItem objects.'
        );
        $paymentSummary = new PaymentSummary(42.50);
        $shippingAddress = new Address('Street 1', 'City', '90210', 'CA', 'USA');
        $paymentAdjustment = new PaymentAdjustment('foo');
        new ReceiptTemplate(
            'foo',
            'ABC1234',
            'EUR',
            'MasterCard xx1234',
            ['foo'],
            $paymentSummary,
            123456,
            'https://example.com',
            $shippingAddress,
            [$paymentAdjustment]
        );
    }

    public function testPaymentAdjustmentsArrayElements()
    {
        $this->setExpectedException(
            'MessageBird\FacebookMessenger\Exceptions\InvalidArgumentException',
            'Payment adjustments array elements must be PaymentAdjustment objects.'
        );
        $receiptItem = new ReceiptItem('foo');
        $paymentSummary = new PaymentSummary(42.50);
        $shippingAddress = new Address('Street 1', 'City', '90210', 'CA', 'USA');
        new ReceiptTemplate(
            'foo',
            'ABC1234',
            'EUR',
            'MasterCard xx1234',
            [$receiptItem],
            $paymentSummary,
            123456,
            'https://example.com',
            $shippingAddress,
            ['foo']
        );
    }

    public function testGetters()
    {
        $receiptItem = new ReceiptItem('foo');
        $paymentSummary = new PaymentSummary(42.50);
        $shippingAddress = new Address('Street 1', 'City', '90210', 'CA', 'USA');
        $paymentAdjustment = new PaymentAdjustment('foo');
        $receiptTemplate = new ReceiptTemplate(
            'foo',
            'ABC1234',
            'EUR',
            'MasterCard xx1234',
            [$receiptItem],
            $paymentSummary,
            123456,
            'https://example.com',
            $shippingAddress,
            [$paymentAdjustment]
        );
        $this->assertEquals('foo', $receiptTemplate->getRecipientName());
        $this->assertEquals('ABC1234', $receiptTemplate->getOrderNumber());
        $this->assertEquals('EUR', $receiptTemplate->getCurrency());
        $this->assertEquals('MasterCard xx1234', $receiptTemplate->getPaymentMethod());
        $receiptItems = $receiptTemplate->getReceiptItems();
        $this->assertInternalType('array', $receiptItems);
        $this->assertNotEmpty($receiptItems);
        $this->assertEquals($receiptItems[0], $receiptItem);
        $this->assertEquals($paymentSummary, $receiptTemplate->getPaymentSummary());
        $this->assertEquals(123456, $receiptTemplate->getTimestamp());
        $this->assertEquals('https://example.com', $receiptTemplate->getOrderUrl());
        $this->assertEquals($shippingAddress, $receiptTemplate->getShippingAddress());
        $paymentAdjustments = $receiptTemplate->getPaymentAdjustments();
        $this->assertInternalType('array', $paymentAdjustments);
        $this->assertNotEmpty($paymentAdjustment);
        $this->assertEquals($paymentAdjustments[0], $paymentAdjustment);
    }

    public function testFormatAsArray()
    {
        $receiptItem = new ReceiptItem('foo');
        $paymentSummary = new PaymentSummary(42.50);
        $shippingAddress = new Address('Street 1', 'City', '90210', 'CA', 'USA');
        $paymentAdjustment = new PaymentAdjustment('foo');
        $receiptTemplate = new ReceiptTemplate(
            'foo',
            'ABC1234',
            'EUR',
            'MasterCard xx1234',
            [$receiptItem],
            $paymentSummary,
            123456,
            'https://example.com',
            $shippingAddress,
            [$paymentAdjustment]
        );
        $this->assertEquals([
            'attachment' => [
                'type' => 'template',
                'payload' => [
                    'template_type' => 'receipt',
                    'recipient_name' => 'foo',
                    'order_number' => 'ABC1234',
                    'currency' => 'EUR',
                    'payment_method' => 'MasterCard xx1234',
                    'timestamp' => 123456,
                    'order_url' => 'https://example.com',
                    'elements' => [$receiptItem->formatAsArray()],
                    'address' => $shippingAddress->formatAsArray(),
                    'summary' => $paymentSummary->formatAsArray(),
                    'adjustments' => [$paymentAdjustment->formatAsArray()],
                ],
            ],
        ], $receiptTemplate->formatAsArray());
    }

    public function testGettersWithoutOptionalArguments()
    {
        $receiptItem = new ReceiptItem('foo');
        $paymentSummary = new PaymentSummary(42.50);
        $receiptTemplate = new ReceiptTemplate(
            'foo',
            'ABC1234',
            'EUR',
            'MasterCard xx1234',
            [$receiptItem],
            $paymentSummary
        );
        $this->assertEquals('foo', $receiptTemplate->getRecipientName());
        $this->assertEquals('ABC1234', $receiptTemplate->getOrderNumber());
        $this->assertEquals('EUR', $receiptTemplate->getCurrency());
        $this->assertEquals('MasterCard xx1234', $receiptTemplate->getPaymentMethod());
        $receiptItems = $receiptTemplate->getReceiptItems();
        $this->assertInternalType('array', $receiptItems);
        $this->assertNotEmpty($receiptItems);
        $this->assertEquals($receiptItems[0], $receiptItem);
        $this->assertEquals($paymentSummary, $receiptTemplate->getPaymentSummary());
        $this->assertEquals(null, $receiptTemplate->getTimestamp());
        $this->assertEquals(null, $receiptTemplate->getOrderUrl());
        $this->assertEquals(null, $receiptTemplate->getShippingAddress());
        $paymentAdjustments = $receiptTemplate->getPaymentAdjustments();
        $this->assertInternalType('array', $paymentAdjustments);
        $this->assertEmpty($paymentAdjustments);
    }

    public function testFormatAsArrayWithoutOptionalArguments()
    {
        $receiptItem = new ReceiptItem('foo');
        $paymentSummary = new PaymentSummary(42.50);
        $receiptTemplate = new ReceiptTemplate(
            'foo',
            'ABC1234',
            'EUR',
            'MasterCard xx1234',
            [$receiptItem],
            $paymentSummary
        );
        $this->assertEquals([
            'attachment' => [
                'type' => 'template',
                'payload' => [
                    'template_type' => 'receipt',
                    'recipient_name' => 'foo',
                    'order_number' => 'ABC1234',
                    'currency' => 'EUR',
                    'payment_method' => 'MasterCard xx1234',
                    'timestamp' => null,
                    'order_url' => null,
                    'elements' => [$receiptItem->formatAsArray()],
                    'address' => null,
                    'summary' => $paymentSummary->formatAsArray(),
                    'adjustments' => null,
                ],
            ],
        ], $receiptTemplate->formatAsArray());
    }
}
