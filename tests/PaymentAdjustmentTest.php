<?php

use MessageBird\FacebookMessenger\Objects\PaymentAdjustment;

class PaymentAdjustmentTest extends PHPUnit_Framework_TestCase
{
    public function testGetters()
    {
        $paymentAdjustment = new PaymentAdjustment('foobar', 42);
        $this->assertEquals('foobar', $paymentAdjustment->getName());
        $this->assertEquals(42, $paymentAdjustment->getAmount());
    }

    public function testGettersWithoutOptionalArguments()
    {
        $paymentAdjustment = new PaymentAdjustment();
        $this->assertEquals(null, $paymentAdjustment->getName());
        $this->assertEquals(null, $paymentAdjustment->getAmount());
    }

    public function testFormatAsArray()
    {
        $paymentAdjustment = new PaymentAdjustment();
        $this->assertEquals([
            'name' => null,
            'amount' => null,
        ], $paymentAdjustment->formatAsArray());
    }
}
