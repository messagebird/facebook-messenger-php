<?php

use MessageBird\FacebookMessenger\Objects\Button;

class ButtonTest extends PHPUnit_Framework_TestCase
{
    public function testGetters()
    {
        $button = new Button('web_url', 'foo', 'https://example.com', 'bar');
        $this->assertEquals('web_url', $button->getType());
        $this->assertEquals('foo', $button->getTitle());
        $this->assertEquals('https://example.com', $button->getUrl());
        $this->assertEquals('bar', $button->getPayload());
        $this->assertInternalType('array', $button->formatAsArray());
        $this->assertEquals([
            'type' => 'web_url',
            'title' => 'foo',
            'url' => 'https://example.com',
            'payload' => 'bar',
        ], $button->formatAsArray());
    }

    public function testGettersWithoutOptionalArguments()
    {
        $button = new Button('web_url', 'foo');
        $this->assertEquals('web_url', $button->getType());
        $this->assertEquals('foo', $button->getTitle());
        $this->assertEquals(null, $button->getUrl());
        $this->assertEquals(null, $button->getPayload());
        $this->assertInternalType('array', $button->formatAsArray());
        $this->assertEquals([
            'type' => 'web_url',
            'title' => 'foo',
            'url' => null,
            'payload' => null,
        ], $button->formatAsArray());
    }
}
