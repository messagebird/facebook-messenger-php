<?php

use GuzzleHttp\Client;
use GuzzleHttp\Exception\BadResponseException;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use MessageBird\FacebookMessenger\Objects\ImageFileMessage;
use MessageBird\FacebookMessenger\Objects\Recipient;
use MessageBird\FacebookMessenger\Objects\TextMessage;
use MessageBird\FacebookMessenger\Requests\SendApiRequest;

class SendApiRequestTest extends PHPUnit_Framework_TestCase
{
    public function testCreateMessage()
    {
        $data = '{"recipient_id": "1008372609250235","message_id":"mid.1456970487936:c34767dfe57ee6e339"}';
        $request = new SendApiRequest('foobar', self::createMockClient(new Response(200, [], $data)));
        $response = $request->createMessage(new Recipient('foo'), new TextMessage('bar'));
        $this->assertInstanceOf('MessageBird\FacebookMessenger\Responses\SendApiResponse', $response);
        $this->assertEquals($data, $response->getBody());
        $this->assertEquals('1008372609250235', $response->getRecipientId());
        $this->assertEquals('mid.1456970487936:c34767dfe57ee6e339', $response->getMessageId());
    }

    public function testCreateFileMessage()
    {
        $data = '{"recipient_id": "1008372609250235","message_id":"mid.1456970487936:c34767dfe57ee6e339"}';
        $request = new SendApiRequest('foobar', self::createMockClient(new Response(200, [], $data)));
        $response = $request->createFileMessage(new Recipient('foo'), new ImageFileMessage('php://temp', 'image.jpg'));

        $this->assertInstanceOf('MessageBird\FacebookMessenger\Responses\SendApiResponse', $response);
        $this->assertEquals($data, $response->getBody());
        $this->assertEquals('1008372609250235', $response->getRecipientId());
        $this->assertEquals('mid.1456970487936:c34767dfe57ee6e339', $response->getMessageId());
    }

    public function testCreateFileMessageWithCustomContentType()
    {
        $data = '{"recipient_id": "1008372609250235","message_id":"mid.1456970487936:c34767dfe57ee6e339"}';
        $request = new SendApiRequest('foobar', self::createMockClient(new Response(200, [], $data)));
        $response = $request->createFileMessage(new Recipient('foo'), new ImageFileMessage(
            'php://temp',
            'image.jpg',
            'foo/bar'
        ));

        $this->assertInstanceOf('MessageBird\FacebookMessenger\Responses\SendApiResponse', $response);
        $this->assertEquals($data, $response->getBody());
        $this->assertEquals('1008372609250235', $response->getRecipientId());
        $this->assertEquals('mid.1456970487936:c34767dfe57ee6e339', $response->getMessageId());
    }

    public function testGetters()
    {
        $this->assertEquals('https://graph.facebook.com/v2.6/me/messages/', SendApiRequest::getResourceUri());
    }

    public function testClientException()
    {
        $this->setExpectedException('MessageBird\FacebookMessenger\Exceptions\ClientException');
        $request = new SendApiRequest('foobar', self::createMockClient(new Response(400, [], '')));
        $request->createMessage(new Recipient('foo'), new TextMessage('bar'));
    }

    public function testServerException()
    {
        $this->setExpectedException('MessageBird\FacebookMessenger\Exceptions\ServerException');
        $request = new SendApiRequest('foobar', self::createMockClient(new Response(500, [], '')));
        $request->createMessage(new Recipient('foo'), new TextMessage('bar'));
    }

    public function testBadResponseException()
    {
        $this->setExpectedException('MessageBird\FacebookMessenger\Exceptions\BadResponseException');
        $request = new \GuzzleHttp\Psr7\Request('POST', 'foo://bar.com');
        $sendApiRequest = new SendApiRequest('foobar', self::createMockClient(
            new BadResponseException('foo', $request)
        ));
        $sendApiRequest->createMessage(new Recipient('foo'), new TextMessage('bar'));
    }

    public function testRequestException()
    {
        $this->setExpectedException('MessageBird\FacebookMessenger\Exceptions\RequestException');
        $request = new \GuzzleHttp\Psr7\Request('POST', 'foo://bar.com');
        $sendApiRequest = new SendApiRequest('foobar', self::createMockClient(
            new RequestException('foo', $request)
        ));
        $sendApiRequest->createMessage(new Recipient('foo'), new TextMessage('bar'));
    }

    protected static function createMockClient($response)
    {
        $mock = new MockHandler([$response]);
        $handler = HandlerStack::create($mock);

        return new Client(['handler' => $handler]);
    }
}
