<?php

use MessageBird\FacebookMessenger\Objects\Attachment;

class AttachmentTest extends PHPUnit_Framework_TestCase
{
    public function testGetters()
    {
        $attachment = new Attachment('image', 'https://example.com/image.jpg', 'foo', 'https://example.com/');

        $this->assertEquals('image', $attachment->getType());
        $this->assertEquals('https://example.com/image.jpg', $attachment->getPayload());
        $this->assertEquals('foo', $attachment->getTitle());
        $this->assertEquals('https://example.com/', $attachment->getUrl());
    }
}
