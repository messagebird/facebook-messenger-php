<?php

use MessageBird\FacebookMessenger\Objects\Address;

class AddressTest extends PHPUnit_Framework_TestCase
{
    public function testGetters()
    {
        $address = new Address(
            'Street 1',
            'City',
            '90210',
            'CA',
            'USA',
            'Street 2'
        );

        $this->assertEquals('Street 1', $address->getStreet1());
        $this->assertEquals('City', $address->getCity());
        $this->assertEquals('90210', $address->getPostalCode());
        $this->assertEquals('CA', $address->getState());
        $this->assertEquals('USA', $address->getCountry());
        $this->assertEquals('Street 2', $address->getStreet2());
        $this->assertInternalType('array', $address->formatAsArray());
        $this->assertEquals([
            'street_1' => 'Street 1',
            'city' => 'City',
            'postal_code' => '90210',
            'state' => 'CA',
            'country' => 'USA',
            'street_2' => 'Street 2',
        ], $address->formatAsArray());
    }

    public function testGettersWithoutOptionalArguments()
    {
        $address = new Address(
            'Street 1',
            'City',
            '90210',
            'CA',
            'USA'
        );

        $this->assertEquals('Street 1', $address->getStreet1());
        $this->assertEquals('City', $address->getCity());
        $this->assertEquals('90210', $address->getPostalCode());
        $this->assertEquals('CA', $address->getState());
        $this->assertEquals('USA', $address->getCountry());
        $this->assertEquals(null, $address->getStreet2());
        $this->assertInternalType('array', $address->formatAsArray());
        $this->assertEquals([
            'street_1' => 'Street 1',
            'city' => 'City',
            'postal_code' => '90210',
            'state' => 'CA',
            'country' => 'USA',
            'street_2' => null,
        ], $address->formatAsArray());
    }
}
