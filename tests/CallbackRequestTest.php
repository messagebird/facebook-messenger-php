<?php
use League\Event\Emitter;
use League\Event\Event;
use MessageBird\FacebookMessenger\CallbackListener;
use MessageBird\FacebookMessenger\Requests\CallbackRequest;

class CallbackRequestTest extends PHPUnit_Framework_TestCase
{
    public function tokenProvider()
    {
        return [
            ['foo', 'foo', true],
            ['foo', 'bar', false],
        ];
    }

    public function signatureProvider()
    {
        return [
            ['85d155c55ed286a300bd1cf124de08d87e914f3a', 'foo', 'bar', true],
            ['baz', 'foo', 'bar', false],
        ];
    }

    public function testCreateWithInvalidJsonBody()
    {
        $this->setExpectedException(
            'MessageBird\FacebookMessenger\Exceptions\InvalidArgumentException',
            'Request body does not contain valid JSON.'
        );
        new CallbackRequest(new Emitter(), 'foo', 'bar', '46b4ec586117154dacd49d664e5d63fdc88efb51');
    }

    public function testHandleAuthenticationCallback()
    {
        $emitter = new Emitter();
        $emitter->addListener('authentication.callback', CallbackListener::fromCallable(
            function ($message, $pageId, $timestamp) {
                $this->assertInstanceOf(
                    'MessageBird\FacebookMessenger\Objects\CallbackAuthenticationMessage',
                    $message
                );
                $this->assertEquals(456, $pageId);
                $this->assertEquals(12341, $timestamp);
            }
        ));
        $data = '{"object":"page","entry":[{"id":456,"time":12341,"messaging":[{"sender":{"id":123},' .
            '"recipient":{"id":456},"timestamp":1234567890,"optin":{"ref":"PASS_THROUGH_PARAM"}}]}]}';
        $request = new CallbackRequest($emitter, 'foobar', $data, '4409bbbc428d375ad47633e353e0e4795fe67591');
        $events = $request->handle();
        $this->assertInternalType('array', $events);
        $this->assertNotEmpty($events);
        $this->assertEquals('authentication.callback', $events[0]->getName());
    }

    public function testHandleMessageReceivedCallback()
    {
        $emitter = new Emitter();
        $emitter->addListener('message.received.callback', CallbackListener::fromCallable(
            function ($message, $pageId, $timestamp) {
                $this->assertInstanceOf('MessageBird\FacebookMessenger\Objects\CallbackReceivedMessage', $message);
                $this->assertEquals(456, $pageId);
                $this->assertEquals(1457764198246, $timestamp);
            }
        ));
        $data = '{"object":"page","entry":[{"id":456,"time":1457764198246,"messaging":[{"sender":{"id":123},' .
            '"recipient":{"id":456},"timestamp":1457764197627,"message":{"mid":' .
            '"mid.1457764197618:41d102a3e1ae206a38","seq":73,"text":"hello, world!"}}]}]}';
        $request = new CallbackRequest($emitter, 'foobar', $data, 'c2ddf2eacb5f1b11db990f0f30723a22c2e02a39');
        $events = $request->handle();
        $this->assertInternalType('array', $events);
        $this->assertNotEmpty($events);
        $this->assertEquals('message.received.callback', $events[0]->getName());
    }

    public function testHandleMultipleMessagesReceivedCallback()
    {
        $emitter = new Emitter();
        $emitter->addListener('message.received.callback', CallbackListener::fromCallable(
            function ($message, $pageId, $timestamp) {
            }
        ));
        $data = '{"object":"page","entry":[{"id":923893911061289,"time":1461581467694,"messaging":[{"sender":{"id"' .
            ':1225604484116394},"recipient":{"id":923893911061289},"timestamp":1461580682932,"message":{"mid":"mid' .
            '.1461580682922:a98a7c38b23cdd6511","seq":34,"text":"Oncoming!"}},{"sender":{"id":1225604484116394},"r' .
            'ecipient":{"id":923893911061289},"timestamp":1461580720880,"message":{"mid":"mid.1461580720868:dd59b4' .
            '8c4b2d29c218","seq":35,"text":"It works?"}},{"sender":{"id":1225604484116394},"recipient":{"id":92389' .
            '3911061289},"timestamp":1461580742742,"message":{"mid":"mid.1461580742733:6ac50044a92c5d5119","seq":3' .
            '6,"text":"Yo"}},{"sender":{"id":1225604484116394},"recipient":{"id":923893911061289},"timestamp":1461' .
            '580784961,"message":{"mid":"mid.1461580784849:07b0a718412ef56f55","seq":37,"text":"INcoming"}},{"send' .
            'er":{"id":1225604484116394},"recipient":{"id":923893911061289},"timestamp":1461580820151,"message":{"' .
            'mid":"mid.1461580820140:fb2231af802d7ec092","seq":38,"text":"Yo"}},{"sender":{"id":1225604484116394},' .
            '"recipient":{"id":923893911061289},"timestamp":1461580843277,"message":{"mid":"mid.1461580843240:92ab' .
            '5630be08908e87","seq":39,"text":"Yo"}},{"sender":{"id":1225604484116394},"recipient":{"id":9238939110' .
            '61289},"timestamp":1461580889768,"message":{"mid":"mid.1461580889757:a53136389f80154784","seq":40,"te' .
            'xt":"It work?"}}]}]}';
        $request = new CallbackRequest($emitter, 'foobar', $data, '0aeca1ad12e8b985a2f5b88f66298411a46a60fa');
        $events = $request->handle();
        $this->assertInternalType('array', $events);
        $this->assertNotEmpty($events);
        $this->assertEquals(7, count($events));
        array_walk($events, function (Event $event) {
            $this->assertEquals('message.received.callback', $event->getName());
        });
    }

    public function testHandleMessageDeliveredCallback()
    {
        $emitter = new Emitter();
        $emitter->addListener(
            'message.delivered.callback',
            CallbackListener::fromCallable(function ($message, $pageId, $timestamp) {
                $this->assertInstanceOf('MessageBird\FacebookMessenger\Objects\CallbackDeliveredMessage', $message);
                $this->assertEquals(456, $pageId);
                $this->assertEquals(1458668856451, $timestamp);
            })
        );
        $data = '{"object":"page","entry":[{"id":456,"time":1458668856451,"messaging":[{"sender":{"id":123},' .
            '"recipient":{"id":456},"delivery":{"mids":["mid.1458668856218:ed81099e15d3f4f233"],' .
            '"watermark":1458668856253,"seq":37}}]}]}';
        $request = new CallbackRequest($emitter, 'foobar', $data, '3baf3d9e6bca6d0b80c66b46ed1368a2ebd4f5ba');
        $events = $request->handle();
        $this->assertInternalType('array', $events);
        $this->assertNotEmpty($events);
        $this->assertEquals('message.delivered.callback', $events[0]->getName());
    }

    public function testHandleWebhookVerificationCallback()
    {
        $emitter = new Emitter();
        $emitter->addListener(
            'webhook.verification.callback',
            CallbackListener::fromCallable(function ($isVerified, $challenge) {
                $this->assertTrue($isVerified);
                $this->assertEquals('bar', $challenge);
            })
        );
        $request = new CallbackRequest($emitter, 'foobar', null, null, 'foo', 'foo', 'bar');
        $events = $request->handle();
        $this->assertInternalType('array', $events);
        $this->assertNotEmpty($events);
        $this->assertEquals('webhook.verification.callback', $events[0]->getName());
    }

    public function testUnsupportedCallback()
    {
        $emitter = new Emitter();
        $emitter->addListener(
            'unsupported.callback',
            CallbackListener::fromCallable(function ($message, $pageId, $timestamp) {
                $this->assertInstanceOf('MessageBird\FacebookMessenger\Objects\CallbackUnsupported', $message);
                $this->assertEquals(456, $pageId);
                $this->assertEquals(1458668856451, $timestamp);
            })
        );
        $data = '{"object":"page","entry":[{"id":456,"time":1458668856451,"messaging":[{"sender":{"id":123},' .
            '"recipient":{"id":456},"foobar":{"mids":["mid.1458668856218:ed81099e15d3f4f233"],' .
            '"watermark":1458668856253,"seq":37}}]}]}';
        $request = new CallbackRequest($emitter, 'foobar', $data, '34aa9bab424db17be14729fc4737e634eb82ef72');
        $events = $request->handle();
        $this->assertInternalType('array', $events);
        $this->assertNotEmpty($events);
        $this->assertEquals('callback.unsupported', $events[0]->getName());
    }

    /**
     * @dataProvider tokenProvider
     */
    public function testVerifyTokenValidity($verifyToken, $validationToken, $result)
    {
        $this->assertEquals($result, CallbackRequest::isValidVerifyToken($verifyToken, $validationToken));
        $this->assertInternalType('bool', CallbackRequest::isValidVerifyToken($verifyToken, $validationToken));
    }

    public function testHandleMissingEntryArrayInBody()
    {
        $this->setExpectedException(
            'MessageBird\FacebookMessenger\Exceptions\DomainException',
            'Request body is missing the `entry` array.'
        );
        $request = new CallbackRequest(
            new Emitter(),
            'foobar',
            '{"foo":"bar"}',
            'c86f366ed26f1e85c98eb0744114ba54fb0a110d'
        );
        $request->handle();
    }

    public function testHandleEntryInvalidTypeInBody()
    {
        $this->setExpectedException(
            'MessageBird\FacebookMessenger\Exceptions\DomainException',
            'The `entry` element in the request body must be an array.'
        );
        $request = new CallbackRequest(
            new Emitter(),
            'foobar',
            '{"entry":"bar"}',
            'b3de0769bcf44b77a3a786540e11ee0fe8f0e329'
        );
        $request->handle();
    }

    /**
     * @dataProvider signatureProvider
     */
    public function testVerifySignatureValidity($signature, $data, $secret, $result)
    {
        $this->assertInternalType('bool', CallbackRequest::isValidSignature($signature, $data, $secret));
        $this->assertEquals($result, CallbackRequest::isValidSignature($signature, $data, $secret));
    }

    public function testMissingSignatureForRequestsWithBody()
    {
        $this->setExpectedException(
            'MessageBird\FacebookMessenger\Exceptions\DomainException',
            'Signature verification failed.'
        );
        $data = '{"object":"page","entry":[{"id":456,"time":12341,"messaging":[{"sender":{"id":123},' .
            '"recipient":{"id":456},"timestamp":1234567890,"optin":{"ref":"PASS_THROUGH_PARAM"}}]}]}';
        $request = new CallbackRequest(new Emitter(), 'foobar', $data);
        $request->handle();
    }
}
