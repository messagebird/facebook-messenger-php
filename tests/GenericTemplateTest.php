<?php

use MessageBird\FacebookMessenger\Objects\Element;
use MessageBird\FacebookMessenger\Objects\GenericTemplate;

class GenericTemplateTest extends PHPUnit_Framework_TestCase
{
    public function testGetters()
    {
        $element = new Element('foobar');
        $genericTemplate = new GenericTemplate([$element]);
        $elements = $genericTemplate->getElements();
        $this->assertInternalType('array', $elements);
        $this->assertNotEmpty($elements);
        $this->assertEquals($element, $elements[0]);
    }

    public function testFormatAsArray()
    {
        $element = new Element('foobar');
        $genericTemplate = new GenericTemplate([$element]);
        $this->assertEquals([
            'attachment' => [
                'type' => 'template',
                'payload' => [
                    'template_type' => 'generic',
                    'elements' => [$element->formatAsArray()],
                ],
            ],
        ], $genericTemplate->formatAsArray());
    }

    public function testEmptyElementsArray()
    {
        $this->setExpectedException(
            'MessageBird\FacebookMessenger\Exceptions\InvalidArgumentException',
            'Elements array is required.'
        );
        new GenericTemplate([]);
    }

    public function testElementsArrayElements()
    {
        $this->setExpectedException(
            'MessageBird\FacebookMessenger\Exceptions\InvalidArgumentException',
            'Elements array elements must be Element objects.'
        );
        new GenericTemplate(['bar']);
    }
}
