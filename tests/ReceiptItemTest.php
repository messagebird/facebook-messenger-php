<?php

use MessageBird\FacebookMessenger\Objects\ReceiptItem;

class ReceiptItemTest extends PHPUnit_Framework_TestCase
{
    public function testGetters()
    {
        $receiptItem = new ReceiptItem(
            'foo',
            'bar',
            42,
            42.50,
            'EUR',
            'https://example.com/image.jpg'
        );
        $this->assertEquals('foo', $receiptItem->getTitle());
        $this->assertEquals('bar', $receiptItem->getSubtitle());
        $this->assertEquals(42, $receiptItem->getQuantity());
        $this->assertEquals(42.50, $receiptItem->getPrice());
        $this->assertEquals('EUR', $receiptItem->getCurrency());
        $this->assertEquals('https://example.com/image.jpg', $receiptItem->getImageUrl());
    }

    public function testFormatAsArray()
    {
        $receiptItem = new ReceiptItem(
            'foo',
            'bar',
            42,
            42.50,
            'EUR',
            'https://example.com/image.jpg'
        );
        $this->assertEquals([
            'title' => 'foo',
            'subtitle' => 'bar',
            'quantity' => 42,
            'price' => 42.50,
            'currency' => 'EUR',
            'image_url' => 'https://example.com/image.jpg'
        ], $receiptItem->formatAsArray());
    }

    public function testGettersWithoutOptionalArguments()
    {
        $receiptItem = new ReceiptItem('foo');
        $this->assertEquals('foo', $receiptItem->getTitle());
        $this->assertEquals(null, $receiptItem->getSubtitle());
        $this->assertEquals(null, $receiptItem->getQuantity());
        $this->assertEquals(null, $receiptItem->getPrice());
        $this->assertEquals(null, $receiptItem->getCurrency());
        $this->assertEquals(null, $receiptItem->getImageUrl());
    }

    public function testFormatAsArrayWithoutOptionalArguments()
    {
        $receiptItem = new ReceiptItem('foo');
        $this->assertEquals([
            'title' => 'foo',
            'subtitle' => null,
            'quantity' => null,
            'price' => null,
            'currency' => null,
            'image_url' => null,
        ], $receiptItem->formatAsArray());
    }
}
