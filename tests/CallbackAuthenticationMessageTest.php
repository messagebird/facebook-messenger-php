<?php

use MessageBird\FacebookMessenger\Objects\CallbackAuthenticationMessage;

class CallbackAuthenticationMessageTest extends PHPUnit_Framework_TestCase
{
    public function testCreateSenderIdMissing()
    {
        $this->setExpectedException(
            'MessageBird\FacebookMessenger\Exceptions\InvalidArgumentException',
            'Sender ID is required.'
        );
        new CallbackAuthenticationMessage([
            'recipient' => ['id' => 456],
            'timestamp' => 1234,
            'optin' => ['ref' => 'foobar'],
        ]);
    }

    public function testCreateRecipientIdMissing()
    {
        $this->setExpectedException(
            'MessageBird\FacebookMessenger\Exceptions\InvalidArgumentException',
            'Recipient ID is required.'
        );
        new CallbackAuthenticationMessage([
            'sender' => ['id' => 123],
            'timestamp' => 1234,
            'optin' => ['ref' => 'foobar'],
        ]);
    }

    public function testCreateTimestampMissing()
    {
        $this->setExpectedException(
            'MessageBird\FacebookMessenger\Exceptions\InvalidArgumentException',
            'Timestamp is required.'
        );
        new CallbackAuthenticationMessage([
            'sender' => ['id' => 123],
            'recipient' => ['id' => 456],
            'optin' => ['ref' => 'foobar'],
        ]);
    }

    public function testCreateOptinRefMissing()
    {
        $this->setExpectedException(
            'MessageBird\FacebookMessenger\Exceptions\InvalidArgumentException',
            'Opt-in reference is required.'
        );
        new CallbackAuthenticationMessage([
            'sender' => ['id' => 123],
            'recipient' => ['id' => 456],
            'timestamp' => 1234,
        ]);
    }

    public function testGetters()
    {
        $message = new CallbackAuthenticationMessage([
            'sender' => ['id' => 123],
            'recipient' => ['id' => 456],
            'timestamp' => 1234,
            'optin' => ['ref' => 'foobar'],
        ]);

        $this->assertEquals(123, $message->getSenderId());
        $this->assertEquals(456, $message->getRecipientId());
        $this->assertEquals(1234, $message->getTimestamp());
        $this->assertEquals('foobar', $message->getOptInReference());
        $this->assertEquals('authentication', $message->getType());
    }
}

