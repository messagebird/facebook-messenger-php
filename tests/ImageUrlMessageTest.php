<?php

use MessageBird\FacebookMessenger\Objects\ImageUrlMessage;

class ImageUrlMessageTest extends PHPUnit_Framework_TestCase
{
    public function testGetters()
    {
        $message = new ImageUrlMessage('https://example.com/image.jpg');
        $this->assertEquals('https://example.com/image.jpg', $message->getUrl());
    }

    public function testFormatAsArray()
    {
        $message = new ImageUrlMessage('https://example.com/image.jpg');
        $this->assertEquals([
            'attachment' => [
                'type' => 'image',
                'payload' => [
                    'url' => 'https://example.com/image.jpg',
                ],
            ],
        ], $message->formatAsArray());
    }
}
