<?php

use GuzzleHttp\Client;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use League\Event\Emitter;
use MessageBird\FacebookMessenger\FacebookMessenger;
use MessageBird\FacebookMessenger\Objects\ImageFileMessage;
use MessageBird\FacebookMessenger\Objects\Recipient;
use MessageBird\FacebookMessenger\Objects\TextMessage;

class FacebookMessengerTest extends PHPUnit_Framework_TestCase
{
    public function testRegisterEventListener()
    {
        $emitter = new Emitter();
        $facebookMessenger = new FacebookMessenger(null, null, null, null, null, null, $emitter);
        $facebookMessenger->on('authentication.callback', function () {
        });
        $this->assertTrue($emitter->hasListeners('authentication.callback'));
    }

    public function testHandle()
    {
        $data = '{"object":"page","entry":[{"id":456,"time":1457764198246,"messaging":[{"sender":{"id":123},' .
            '"recipient":{"id":456},"timestamp":1457764197627,"message":{"mid":' .
            '"mid.1457764197618:41d102a3e1ae206a38","seq":73,"text":"hello, world!"}}]}]}';
        $facebookMessenger = new FacebookMessenger(
            null,
            'foobar',
            null,
            null,
            null,
            null,
            null,
            $data,
            'c2ddf2eacb5f1b11db990f0f30723a22c2e02a39'
        );
        $events = $facebookMessenger->handle();
        $this->assertInternalType('array', $events);
        $this->assertNotEmpty($events);
        $this->assertInstanceOf('League\Event\Event', $events[0]);
    }

    /**
     * @backupGlobals enabled
     */
    public function testGetValidSignatureFromHeader()
    {
        $_SERVER['HTTP_X_HUB_SIGNATURE'] = 'sha1=foo';
        $this->assertEquals('foo', FacebookMessenger::getSignatureFromHeader());
    }

    /**
     * @backupGlobals enabled
     */
    public function testGetInvalidSignatureFromHeader()
    {
        $this->setExpectedException(
            'MessageBird\FacebookMessenger\Exceptions\DomainException',
            'X-Hub-Signature header has invalid value.'
        );
        $_SERVER['HTTP_X_HUB_SIGNATURE'] = 'foo';
        FacebookMessenger::getSignatureFromHeader();
    }

    public function testGetAccessToken()
    {
        $facebookMessenger = new FacebookMessenger('foobar');
        $this->assertEquals('foobar', $facebookMessenger->getAccessToken());
    }

    public function testSetAccessToken()
    {
        $facebookMessenger = new FacebookMessenger();
        $facebookMessenger->setAccessToken('foobar');
        $this->assertEquals('foobar', $facebookMessenger->getAccessToken());
    }

    public function testCreateTextMessage()
    {
        $data = '{"recipient_id": "1008372609250235","message_id":"mid.1456970487936:c34767dfe57ee6e339"}';
        $facebookMessenger = new FacebookMessenger(
            'foobar',
            null,
            null,
            null,
            null,
            self::createMockClient(new Response(200, [], $data))
        );
        $response = $facebookMessenger->sendMessage(new Recipient('1008372609250235'), new TextMessage('foobar'));
        $this->assertInstanceOf('MessageBird\FacebookMessenger\Responses\SendApiResponse', $response);
    }

    public function testCreateImageFileMessage()
    {
        $data = '{"recipient_id": "1008372609250235","message_id":"mid.1456970487936:c34767dfe57ee6e339"}';
        $facebookMessenger = new FacebookMessenger(
            'foobar',
            null,
            null,
            null,
            null,
            self::createMockClient(new Response(200, [], $data))
        );
        $response = $facebookMessenger->sendMessage(
            new Recipient('1008372609250235'),
            new ImageFileMessage('php://temp', 'image.jpg')
        );
        $this->assertInstanceOf('MessageBird\FacebookMessenger\Responses\SendApiResponse', $response);
    }

    public function testCreateMessageWithoutAccessToken()
    {
        $this->setExpectedException(
            'DomainException',
            'Access token is required.'
        );
        $data = '{"recipient_id": "1008372609250235","message_id":"mid.1456970487936:c34767dfe57ee6e339"}';
        $facebookMessenger = new FacebookMessenger(
            null,
            null,
            null,
            null,
            null,
            self::createMockClient(new Response(200, [], $data))
        );
        $facebookMessenger->sendMessage(new Recipient('1008372609250235'), new TextMessage('foobar'));
    }

    public function testGetUserProfile()
    {
        $data = '{"first_name":"foo","last_name":"bar","profile_pic":"https://example.com/image.jpg"}';
        $facebookMessenger = new FacebookMessenger(
            'foobar',
            null,
            null,
            null,
            null,
            self::createMockClient(new Response(200, [], $data))
        );
        $response = $facebookMessenger->getUserProfile(1234);
        $this->assertInstanceOf('MessageBird\FacebookMessenger\Responses\UserProfileApiResponse', $response);
    }

    public function testGetUserProfileWithoutAccessToken()
    {
        $this->setExpectedException(
            'MessageBird\FacebookMessenger\Exceptions\DomainException',
            'Access token is required.'
        );
        $data = '{"first_name":"foo","last_name":"bar","profile_pic":"https://example.com/image.jpg"}';
        $facebookMessenger = new FacebookMessenger(
            null,
            null,
            null,
            null,
            null,
            self::createMockClient(new Response(200, [], $data))
        );
        $response = $facebookMessenger->getUserProfile(1234);
        $this->assertInstanceOf('MessageBird\FacebookMessenger\Responses\UserProfileApiResponse', $response);
    }

    public function testSetWelcomeMessage()
    {
        $data = '{"result":"Successfully added new_thread\'s CTAs"}';
        $facebookMessenger = new FacebookMessenger(
            'foobar',
            null,
            null,
            null,
            null,
            self::createMockClient(new Response(200, [], $data))
        );
        $response = $facebookMessenger->setWelcomeMessage(1234, new TextMessage('foobar'));
        $this->assertInstanceOf('MessageBird\FacebookMessenger\Responses\ThreadSettingsResponse', $response);
    }

    public function testSetWelcomeMessageWithoutAccessToken()
    {
        $this->setExpectedException(
            'MessageBird\FacebookMessenger\Exceptions\DomainException',
            'Access token is required.'
        );
        $data = '{"result":"Successfully added new_thread\'s CTAs"}';
        $facebookMessenger = new FacebookMessenger(
            null,
            null,
            null,
            null,
            null,
            self::createMockClient(new Response(200, [], $data))
        );
        $response = $facebookMessenger->setWelcomeMessage(1234, new TextMessage('foobar'));
        $this->assertInstanceOf('MessageBird\FacebookMessenger\Responses\ThreadSettingsResponse', $response);
    }

    public function testDeleteWelcomeMessage()
    {
        $data = '{"result":"Successfully removed all new_thread\'s CTAs"}';
        $facebookMessenger = new FacebookMessenger(
            'foobar',
            null,
            null,
            null,
            null,
            self::createMockClient(new Response(200, [], $data))
        );
        $response = $facebookMessenger->deleteWelcomeMessage(1234);
        $this->assertInstanceOf('MessageBird\FacebookMessenger\Responses\ThreadSettingsResponse', $response);
    }

    public function testDeleteWelcomeMessageWithoutAccessToken()
    {
        $this->setExpectedException(
            'MessageBird\FacebookMessenger\Exceptions\DomainException',
            'Access token is required.'
        );
        $data = '{"result":"Successfully removed all new_thread\'s CTAs"}';
        $facebookMessenger = new FacebookMessenger(
            null,
            null,
            null,
            null,
            null,
            self::createMockClient(new Response(200, [], $data))
        );
        $response = $facebookMessenger->deleteWelcomeMessage(1234);
        $this->assertInstanceOf('MessageBird\FacebookMessenger\Responses\ThreadSettingsResponse', $response);
    }

    protected static function createMockClient(Response $response)
    {
        $mock = new MockHandler([$response]);
        $handler = HandlerStack::create($mock);

        return new Client(['handler' => $handler]);
    }
}
