<?php

use MessageBird\FacebookMessenger\Objects\VideoUrlMessage;

class VideoUrlMessageTest extends PHPUnit_Framework_TestCase
{
    public function testGetters()
    {
        $message = new VideoUrlMessage('https://example.com/video.mp3');
        $this->assertEquals('https://example.com/video.mp3', $message->getUrl());
    }

    public function testFormatAsArray()
    {
        $message = new VideoUrlMessage('https://example.com/video.mp3');
        $this->assertEquals([
            'attachment' => [
                'type' => 'video',
                'payload' => [
                    'url' => 'https://example.com/video.mp3',
                ],
            ],
        ], $message->formatAsArray());
    }
}
