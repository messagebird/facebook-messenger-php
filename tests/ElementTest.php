<?php

use MessageBird\FacebookMessenger\Objects\Button;
use MessageBird\FacebookMessenger\Objects\Element;

class ElementTest extends PHPUnit_Framework_TestCase
{
    public function testGetters()
    {
        $button = new Button('web_url', 'foobar');
        $element = new Element('foo', 'https://example.com', 'https://example.com/image.jpg', 'bar', [$button]);
        $this->assertEquals('foo', $element->getTitle());
        $this->assertEquals('https://example.com', $element->getItemUrl());
        $this->assertEquals('https://example.com/image.jpg', $element->getImageUrl());
        $this->assertEquals('bar', $element->getSubtitle());
        $buttons = $element->getButtons();
        $this->assertInternalType('array', $buttons);
        $this->assertNotEmpty($buttons);
        $this->assertEquals($button, $buttons[0]);
    }

    public function testGettersWithoutOptionalArguments()
    {
        $element = new Element('foo');
        $this->assertEquals('foo', $element->getTitle());
        $this->assertEquals(null, $element->getItemUrl());
        $this->assertEquals(null, $element->getImageUrl());
        $this->assertEquals(null, $element->getSubtitle());
        $buttons = $element->getButtons();
        $this->assertInternalType('array', $buttons);
        $this->assertEmpty($buttons);
    }

    public function testFormatAsArray()
    {
        $button = new Button('web_url', 'foobar');
        $element = new Element('foo', 'https://example.com', 'https://example.com/image.jpg', 'bar', [$button]);
        $this->assertEquals([
            'title' => 'foo',
            'item_url' => 'https://example.com',
            'image_url' => 'https://example.com/image.jpg',
            'subtitle' => 'bar',
            'buttons' => [$button->formatAsArray()],
        ], $element->formatAsArray());
    }

    public function testFormatAsArrayWithoutOptionalArguments()
    {
        $element = new Element('foo');
        $this->assertEquals([
            'title' => 'foo',
            'item_url' => null,
            'image_url' => null,
            'subtitle' => null,
            'buttons' => null,
        ], $element->formatAsArray());
    }


    public function testButtonsArrayElements()
    {
        $this->setExpectedException(
            'MessageBird\FacebookMessenger\Exceptions\InvalidArgumentException',
            'Buttons array elements must be Button objects.'
        );
        new Element('foo', null, null, null, ['bar']);
    }
}
