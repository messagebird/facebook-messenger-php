<?php

use MessageBird\FacebookMessenger\Objects\VideoFileMessage;

class VideoFileMessageTest extends PHPUnit_Framework_TestCase
{
    public function testGetters()
    {
        $message = new VideoFileMessage('php://temp', 'video.mp4', 'foo/bar');
        $this->assertInstanceOf('GuzzleHttp\Psr7\Stream', $message->getStream());
        $this->assertEquals('video.mp4', $message->getFilename());
        $this->assertEquals('foo/bar', $message->getContentType());
    }

    public function testFormatAsArray()
    {
        $message = new VideoFileMessage('php://temp', 'video.mp4');
        $this->assertEquals([
            'attachment' => [
                'type' => 'video',
                'payload' => [],
            ],
        ], $message->formatAsArray());
    }
}
