<?php

use GuzzleHttp\Psr7\Response;
use MessageBird\FacebookMessenger\Responses\UserProfileApiResponse;

class UserProfileApiResponseTest extends PHPUnit_Framework_TestCase
{
    public function testMissingFirstName()
    {
        $this->setExpectedException(
            'MessageBird\FacebookMessenger\Exceptions\DomainException',
            'First name is required.'
        );
        $data = '{"last_name":"bar","profile_pic":"https://example.com/image.jpg"}';
        $response = new Response(200, [], $data);
        new UserProfileApiResponse($response);
    }

    public function testMissingMessageId()
    {
        $this->setExpectedException(
            'MessageBird\FacebookMessenger\Exceptions\DomainException',
            'Last name is required.'
        );
        $data = '{"first_name":"foo","profile_pic":"https://example.com/image.jpg"}';
        $response = new Response(200, [], $data);
        new UserProfileApiResponse($response);
    }

    public function testMissingProfilePic()
    {
        $this->setExpectedException(
            'MessageBird\FacebookMessenger\Exceptions\DomainException',
            'Profile pic is required.'
        );
        $data = '{"first_name":"foo","last_name":"bar"}';
        $response = new Response(200, [], $data);
        new UserProfileApiResponse($response);
    }


    public function testGetters()
    {
        $data = '{"first_name":"foo","last_name":"bar","profile_pic":"https://example.com/image.jpg"}';
        $response = new Response(200, [], $data);
        $userProfileApiResponse = new UserProfileApiResponse($response);
        $this->assertEquals($data, $userProfileApiResponse->getBody());
        $this->assertEquals('foo', $userProfileApiResponse->getFirstName());
        $this->assertEquals('bar', $userProfileApiResponse->getLastName());
        $this->assertEquals('https://example.com/image.jpg', $userProfileApiResponse->getProfilePictureUrl());
    }
}
