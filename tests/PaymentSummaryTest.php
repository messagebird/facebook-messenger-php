<?php

use MessageBird\FacebookMessenger\Objects\PaymentSummary;

class PaymentSummaryTest extends PHPUnit_Framework_TestCase
{
    public function testGetters()
    {
        $paymentSummary = new PaymentSummary(12.34, 23.45, 34.56, 45.67);
        $this->assertEquals(12.34, $paymentSummary->getTotalCost());
        $this->assertEquals(23.45, $paymentSummary->getSubTotal());
        $this->assertEquals(34.56, $paymentSummary->getShippingCost());
        $this->assertEquals(45.67, $paymentSummary->getTotalTax());
    }

    public function testFormatAsArray()
    {
        $paymentSummary = new PaymentSummary(12.34, 23.45, 34.56, 45.67);
        $this->assertEquals([
            'subtotal' => 23.45,
            'shipping_cost' => 34.56,
            'total_tax' => 45.67,
            'total_cost' => 12.34,
        ], $paymentSummary->formatAsArray());
    }

    public function testGettersWithoutOptionalArguments()
    {
        $paymentSummary = new PaymentSummary(12.34);
        $this->assertEquals(12.34, $paymentSummary->getTotalCost());
        $this->assertEquals(null, $paymentSummary->getSubTotal());
        $this->assertEquals(null, $paymentSummary->getShippingCost());
        $this->assertEquals(null, $paymentSummary->getTotalTax());
    }

    public function testFormatAsArrayWithoutOptionalArguments()
    {
        $paymentSummary = new PaymentSummary(12.34);
        $this->assertEquals([
            'subtotal' => null,
            'shipping_cost' => null,
            'total_tax' => null,
            'total_cost' => 12.34,
        ], $paymentSummary->formatAsArray());
    }
}
