<?php

use MessageBird\FacebookMessenger\Objects\CallbackDeliveredMessage;

class CallbackDeliveredMessageTest extends PHPUnit_Framework_TestCase
{
    public function testCreateSenderIdMissing()
    {
        $this->setExpectedException(
            'MessageBird\FacebookMessenger\Exceptions\InvalidArgumentException',
            'Sender ID is required.'
        );
        new CallbackDeliveredMessage([
            'recipient' => ['id' => 456],
            'delivery' => [
                'mids' => ['mid.1458668856218:ed81099e15d3f4f233'],
                'watermark' => 1458668856253,
                'seq' => 37,
            ],
        ]);
    }

    public function testCreateRecipientIdMissing()
    {
        $this->setExpectedException(
            'MessageBird\FacebookMessenger\Exceptions\InvalidArgumentException',
            'Recipient ID is required.'
        );
        new CallbackDeliveredMessage([
            'sender' => ['id' => 123],
            'delivery' => [
                'mids' => ['mid.1458668856218:ed81099e15d3f4f233'],
                'watermark' => 1458668856253,
                'seq' => 37,
            ],
        ]);
    }

    public function testCreateWatermarkMissing()
    {
        $this->setExpectedException(
            'MessageBird\FacebookMessenger\Exceptions\InvalidArgumentException',
            'Watermark is required.'
        );
        new CallbackDeliveredMessage([
            'sender' => ['id' => 123],
            'recipient' => ['id' => 456],
            'delivery' => [
                'mids' => ['mid.1458668856218:ed81099e15d3f4f233'],
                'seq' => 37,
            ],
        ]);
    }

    public function testCreateSeqMissing()
    {
        $this->setExpectedException(
            'MessageBird\FacebookMessenger\Exceptions\InvalidArgumentException',
            'Seq is required.'
        );
        new CallbackDeliveredMessage([
            'sender' => ['id' => 123],
            'recipient' => ['id' => 456],
            'delivery' => [
                'mids' => ['mid.1458668856218:ed81099e15d3f4f233'],
                'watermark' => 1458668856253,
            ],
        ]);
    }

    public function testGetters()
    {
        $message = new CallbackDeliveredMessage([
            'sender' => ['id' => 123],
            'recipient' => ['id' => 456],
            'delivery' => [
                'mids' => ['mid.1458668856218:ed81099e15d3f4f233'],
                'watermark' => 1458668856253,
                'seq' => 37,
            ],
        ]);

        $this->assertEquals(123, $message->getSenderId());
        $this->assertEquals(456, $message->getRecipientId());
        $this->assertEquals(['mid.1458668856218:ed81099e15d3f4f233'], $message->getMids());
        $this->assertEquals(1458668856253, $message->getWatermark());
        $this->assertEquals(37, $message->getSequenceNumber());
        $this->assertEquals('delivered_message', $message->getType());
    }

    public function testGettersWithMidsMissing()
    {
        $message = new CallbackDeliveredMessage([
            'sender' => ['id' => 123],
            'recipient' => ['id' => 456],
            'delivery' => [
                'watermark' => 1458668856253,
                'seq' => 37,
            ],
        ]);

        $this->assertEquals(123, $message->getSenderId());
        $this->assertEquals(456, $message->getRecipientId());
        $this->assertEquals([], $message->getMids());
        $this->assertEquals(1458668856253, $message->getWatermark());
        $this->assertEquals(37, $message->getSequenceNumber());
        $this->assertEquals('delivered_message', $message->getType());
    }
}
