<?php

use MessageBird\FacebookMessenger\Objects\Button;
use MessageBird\FacebookMessenger\Objects\ButtonTemplate;

class ButtonTemplateTest extends PHPUnit_Framework_TestCase
{
    public function testGetters()
    {
        $button = new Button('web_url', 'foobar', 'https://example.com');
        $buttonTemplate = new ButtonTemplate('foobar', [$button]);
        $this->assertEquals('foobar', $buttonTemplate->getText());
        $buttons = $buttonTemplate->getButtons();
        $this->assertInternalType('array', $buttons);
        $this->assertNotEmpty($buttons);
        $this->assertEquals($button, $buttons[0]);
    }

    public function testFormatAsArray()
    {
        $button = new Button('web_url', 'foobar', 'https://example.com');
        $buttonTemplate = new ButtonTemplate('foobar', [$button]);
        $this->assertEquals([
            'attachment' => [
                'type' => 'template',
                'payload' => [
                    'template_type' => 'button',
                    'text' => 'foobar',
                    'buttons' => [$button->formatAsArray()],
                ],
            ],
        ], $buttonTemplate->formatAsArray());
    }

    public function testEmptyButtonsArray()
    {
        $this->setExpectedException(
            'MessageBird\FacebookMessenger\Exceptions\InvalidArgumentException',
            'Buttons array is required.'
        );
        new ButtonTemplate('foobar', []);
    }

    public function testButtonsArrayElements()
    {
        $this->setExpectedException(
            'MessageBird\FacebookMessenger\Exceptions\InvalidArgumentException',
            'Buttons array elements must be Button objects.'
        );
        new ButtonTemplate('foo', ['bar']);
    }
}
