<?php

use MessageBird\FacebookMessenger\Objects\TextMessage;

class TextMessageTest extends PHPUnit_Framework_TestCase
{
    public function testGetters()
    {
        $message = new TextMessage('foobar');
        $this->assertEquals('foobar', $message->getText());
    }

    public function testFormatAsArray()
    {
        $message = new TextMessage('foobar');
        $this->assertEquals([
            'text' => 'foobar',
        ], $message->formatAsArray());
    }
}
