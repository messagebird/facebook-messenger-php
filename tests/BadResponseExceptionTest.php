<?php

use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Response;
use MessageBird\FacebookMessenger\Exceptions\BadResponseException;

class BadResponseExceptionTest extends PHPUnit_Framework_TestCase
{
    public function testGetErrorMessage()
    {
        $request = new Request('POST', 'Foo');
        $response = new Response(400, [], '{"error":{"message":"foo"}}');

        $e = BadResponseException::create($request, $response);
        $this->assertEquals('foo', $e->getErrorMessage());
    }

    public function testGetErrorType()
    {
        $request = new Request('POST', 'Foo');
        $response = new Response(400, [], '{"error":{"type":"foo"}}');

        $e = BadResponseException::create($request, $response);
        $this->assertEquals('foo', $e->getErrorType());
    }

    public function testGetErrorCode()
    {
        $request = new Request('POST', 'Foo');
        $response = new Response(400, [], '{"error":{"code":190}}');

        $e = BadResponseException::create($request, $response);
        $this->assertEquals(190, $e->getErrorCode());
    }

    public function testGetErrorData()
    {
        $request = new Request('POST', 'Foo');
        $response = new Response(400, [], '{"error":{"error_data":"foo"}}');

        $e = BadResponseException::create($request, $response);
        $this->assertEquals('foo', $e->getErrorData());
    }

    public function testGetFbTraceId()
    {
        $request = new Request('POST', 'Foo');
        $response = new Response(400, [], '{"error":{"fbtrace_id":"foo"}}');

        $e = BadResponseException::create($request, $response);
        $this->assertEquals('foo', $e->getErrorFbTraceId());
    }
}
