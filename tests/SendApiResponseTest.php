<?php

use GuzzleHttp\Psr7\Response;
use MessageBird\FacebookMessenger\Responses\SendApiResponse;

class SendApiResponseTest extends PHPUnit_Framework_TestCase
{
    public function testMissingRecipientId()
    {
        $this->setExpectedException(
            'MessageBird\FacebookMessenger\Exceptions\DomainException',
            'Recipient ID is required.'
        );
        $data = '{"message_id":"mid.1456970487936:c34767dfe57ee6e339"}';
        $response = new Response(200, [], $data);
        new SendApiResponse($response);
    }

    public function testMissingMessageId()
    {
        $this->setExpectedException(
            'MessageBird\FacebookMessenger\Exceptions\DomainException',
            'Message ID is required.'
        );
        $data = '{"recipient_id":"1008372609250235"}';
        $response = new Response(200, [], $data);
        new SendApiResponse($response);
    }

    public function testInvalidBody()
    {
        $this->setExpectedException(
            'MessageBird\FacebookMessenger\Exceptions\DomainException',
            'Body contains invalid JSON data.'
        );
        $data = 'foobar';
        $response = new Response(200, [], $data);
        new SendApiResponse($response);
    }

    public function testGetters()
    {
        $data = '{"recipient_id":"1008372609250235","message_id":"mid.1456970487936:c34767dfe57ee6e339"}';
        $response = new Response(200, [], $data);
        $sendApiResponse = new SendApiResponse($response);
        $this->assertEquals($data, $sendApiResponse->getBody());
        $this->assertEquals('1008372609250235', $sendApiResponse->getRecipientId());
        $this->assertEquals('mid.1456970487936:c34767dfe57ee6e339', $sendApiResponse->getMessageId());
    }
}
