<?php

use MessageBird\FacebookMessenger\Objects\ImageFileMessage;

class ImageFileMessageTest extends PHPUnit_Framework_TestCase
{
    public function testGetters()
    {
        $message = new ImageFileMessage('php://temp', 'image.jpg', 'foo/bar');
        $this->assertInstanceOf('GuzzleHttp\Psr7\Stream', $message->getStream());
        $this->assertEquals('image.jpg', $message->getFilename());
        $this->assertEquals('foo/bar', $message->getContentType());
    }

    public function testFormatAsArray()
    {
        $message = new ImageFileMessage('php://temp', 'image.jpg');
        $this->assertEquals([
            'attachment' => [
                'type' => 'image',
                'payload' => [],
            ],
        ], $message->formatAsArray());
    }
}
