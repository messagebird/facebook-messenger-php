<?php

use MessageBird\FacebookMessenger\Objects\ReceivedAttachmentMessage;

class ReceivedAttachmentMessageTest extends PHPUnit_Framework_TestCase
{
    public function testEmptyAttachments()
    {
        $this->setExpectedException(
            'MessageBird\FacebookMessenger\Exceptions\InvalidArgumentException',
            'Attachments is required.'
        );
        new ReceivedAttachmentMessage('mid.1458696618141:b4ef9d19ec21086067', 51, []);
    }

    public function testAttachmentsMissingType()
    {
        $this->setExpectedException(
            'MessageBird\FacebookMessenger\Exceptions\InvalidArgumentException',
            'Attachment type is required.'
        );
        new ReceivedAttachmentMessage('mid.1458696618141:b4ef9d19ec21086067', 51, [
            [
                'payload' => 'https://example.com/image.jpg',
            ]
        ]);
    }

    public function testAttachmentsMissingPayload()
    {
        $this->setExpectedException(
            'MessageBird\FacebookMessenger\Exceptions\InvalidArgumentException',
            'Attachment payload is required.'
        );
        new ReceivedAttachmentMessage('mid.1458696618141:b4ef9d19ec21086067', 51, [
            [
                'type' => 'image',
            ]
        ]);
    }

    public function testGetters()
    {
        $message = new ReceivedAttachmentMessage('mid.1458696618141:b4ef9d19ec21086067', 51, [
            [
                'type' => 'image',
                'payload' => 'https://example.com/image.jpg',
            ]
        ]);
        $attachments = $message->getAttachments();
        $this->assertInternalType('array', $attachments);
        $this->assertNotEmpty($attachments);
        $this->assertInstanceOf('MessageBird\FacebookMessenger\Objects\Attachment', $attachments[0]);
    }
}
