<?php

use GuzzleHttp\Client;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use MessageBird\FacebookMessenger\Requests\UserProfileApiRequest;

class UserProfileApiRequestTest extends PHPUnit_Framework_TestCase
{
    public function testExecute()
    {
        $data = '{"first_name":"foo","last_name":"bar","profile_pic":"https://example.com/image.jpg"}';
        $request = new UserProfileApiRequest('foobar', self::createMockClient(new Response(200, [], $data)));
        $response = $request->get(1234);
        $this->assertInstanceOf('MessageBird\FacebookMessenger\Responses\UserProfileApiResponse', $response);
        $this->assertEquals($data, $response->getBody());
        $this->assertEquals('foo', $response->getFirstName());
        $this->assertEquals('bar', $response->getLastName());
        $this->assertEquals('https://example.com/image.jpg', $response->getProfilePictureUrl());
    }

    public function testGetters()
    {
        $this->assertEquals('https://graph.facebook.com/v2.6/', UserProfileApiRequest::getResourceUri());
    }

    protected static function createMockClient(Response $response)
    {
        $mock = new MockHandler([$response]);
        $handler = HandlerStack::create($mock);

        return new Client(['handler' => $handler]);
    }
}
