<?php

use MessageBird\FacebookMessenger\Objects\CallbackUnsupported;

class CallbackUnsupportedTest extends PHPUnit_Framework_TestCase
{
    public function testGetters()
    {
        $message = new CallbackUnsupported(['foo' => 'bar']);
        $this->assertEquals(['foo' => 'bar'], $message->getContents());
        $this->assertEquals('callback_unsupported', $message->getType());
    }
}
