<?php

use GuzzleHttp\Client;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use MessageBird\FacebookMessenger\Objects\TextMessage;
use MessageBird\FacebookMessenger\Requests\WelcomeMessageRequest;

class WelcomeMessageRequestTest extends PHPUnit_Framework_TestCase
{
    public function testCreateWelcomeMessage()
    {
        $data = '{"result":"Successfully added new_thread\'s CTAs"}';
        $request = new WelcomeMessageRequest('foobar', self::createMockClient(new Response(200, [], $data)));
        $message = new TextMessage('foobar');
        $response = $request->create(1234, $message);
        $this->assertInstanceOf('MessageBird\FacebookMessenger\Responses\ThreadSettingsResponse', $response);
        $this->assertEquals($data, $response->getBody());
    }

    public function testDeleteWelcomeMessage()
    {
        $data = '{"result":"Successfully removed all new_thread\'s CTAs"}';
        $request = new WelcomeMessageRequest('foobar', self::createMockClient(new Response(200, [], $data)));
        $response = $request->delete(1234);
        $this->assertInstanceOf('MessageBird\FacebookMessenger\Responses\ThreadSettingsResponse', $response);
        $this->assertEquals($data, $response->getBody());
    }

    public function testGetters()
    {
        $this->assertEquals('https://graph.facebook.com/v2.6/', WelcomeMessageRequest::getResourceUri());
    }

    protected static function createMockClient(Response $response)
    {
        $mock = new MockHandler([$response]);
        $handler = HandlerStack::create($mock);

        return new Client(['handler' => $handler]);
    }
}
