<?php

use MessageBird\FacebookMessenger\Objects\CallbackReceivedMessage;

class CallbackReceivedMessageTest extends PHPUnit_Framework_TestCase
{
    public function testCreateSenderIdMissing()
    {
        $this->setExpectedException(
            'MessageBird\FacebookMessenger\Exceptions\InvalidArgumentException',
            'Sender ID is required.'
        );
        new CallbackReceivedMessage([
            'recipient' => ['id' => 456],
            'timestamp' => 1234,
            'optin' => ['ref' => 'foobar'],
        ]);
    }

    public function testCreateRecipientIdMissing()
    {
        $this->setExpectedException(
            'MessageBird\FacebookMessenger\Exceptions\InvalidArgumentException',
            'Recipient ID is required.'
        );
        new CallbackReceivedMessage([
            'sender' => ['id' => 123],
            'timestamp' => 1234,
            'optin' => ['ref' => 'foobar'],
        ]);
    }

    public function testCreateTimestampMissing()
    {
        $this->setExpectedException(
            'MessageBird\FacebookMessenger\Exceptions\InvalidArgumentException',
            'Timestamp is required.'
        );
        new CallbackReceivedMessage([
            'sender' => ['id' => 123],
            'recipient' => ['id' => 456],
            'optin' => ['ref' => 'foobar'],
        ]);
    }

    public function testCreateMessageMissing()
    {
        $this->setExpectedException(
            'MessageBird\FacebookMessenger\Exceptions\InvalidArgumentException',
            'Message is required.'
        );
        new CallbackReceivedMessage([
            'sender' => ['id' => 123],
            'recipient' => ['id' => 456],
            'timestamp' => 1234,
        ]);
    }

    public function testCreateMessageMidMissing()
    {
        $this->setExpectedException(
            'MessageBird\FacebookMessenger\Exceptions\InvalidArgumentException',
            'Message mid is required.'
        );
        new CallbackReceivedMessage([
            'sender' => ['id' => 123],
            'recipient' => ['id' => 456],
            'timestamp' => 1234,
            'message' => [
                'seq' => 73,
                'text' => 'foobar',
            ],
        ]);
    }

    public function testCreateMessageSeqMissing()
    {
        $this->setExpectedException(
            'MessageBird\FacebookMessenger\Exceptions\InvalidArgumentException',
            'Message seq is required.'
        );
        new CallbackReceivedMessage([
            'sender' => ['id' => 123],
            'recipient' => ['id' => 456],
            'timestamp' => 1234,
            'message' => [
                'mid' => 'mid.1457764197618:41d102a3e1ae206a38',
                'text' => 'foobar',
            ],
        ]);
    }

    public function testCreateMessageAttachmentInvalidType()
    {
        $this->setExpectedException(
            'MessageBird\FacebookMessenger\Exceptions\InvalidArgumentException',
            'Message attachments must be an array.'
        );
        new CallbackReceivedMessage([
            'sender' => ['id' => 123],
            'recipient' => ['id' => 456],
            'timestamp' => 1234,
            'message' => [
                'mid' => 'mid.1457764197618:41d102a3e1ae206a38',
                'seq' => 73,
                'attachments' => 'foobar',
            ],
        ]);
    }

    public function testCreateMessageContentMissing()
    {
        $this->setExpectedException(
            'MessageBird\FacebookMessenger\Exceptions\InvalidArgumentException',
            'Message text or attachment is required.'
        );
        new CallbackReceivedMessage([
            'sender' => ['id' => 123],
            'recipient' => ['id' => 456],
            'timestamp' => 1234,
            'message' => [
                'mid' => 'mid.1457764197618:41d102a3e1ae206a38',
                'seq' => 73,
            ],
        ]);
    }

    public function testGettersTextMessage()
    {
        $message = new CallbackReceivedMessage([
            'sender' => ['id' => 123],
            'recipient' => ['id' => 456],
            'timestamp' => 1234,
            'message' => [
                'mid' => 'mid.1457764197618:41d102a3e1ae206a38',
                'seq' => 73,
                'text' => 'foobar',
            ],
        ]);

        $this->assertEquals(123, $message->getSenderId());
        $this->assertEquals(456, $message->getRecipientId());
        $this->assertEquals(1234, $message->getTimestamp());
        $this->assertInstanceOf(
            'MessageBird\FacebookMessenger\Objects\ReceivedTextMessage',
            $message->getMessage()
        );
        $this->assertEquals('received_message', $message->getType());
    }

    public function testGetterImageMessage()
    {
        $message = new CallbackReceivedMessage([
            'sender' => ['id' => 123],
            'recipient' => ['id' => 456],
            'timestamp' => 1234,
            'message' => [
                'mid' => 'mid.1457764197618:41d102a3e1ae206a38',
                'seq' => 73,
                'attachments' => [
                    [
                        'type' => 'image',
                        'payload' => [
                            'url' => 'https://example.com/image.jpg',
                        ],
                    ],
                ],
            ],
        ]);

        $this->assertInstanceOf(
            'MessageBird\FacebookMessenger\Objects\ReceivedAttachmentMessage',
            $message->getMessage()
        );
    }
}
