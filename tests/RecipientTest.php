<?php

use MessageBird\FacebookMessenger\Objects\Recipient;

class RecipientTest extends PHPUnit_Framework_TestCase
{
    public function testMissingArguments()
    {
        $this->setExpectedException(
            'MessageBird\FacebookMessenger\Exceptions\BadFunctionCallException',
            'Id or phone number are required.'
        );
        new Recipient();
    }

    public function testGettersWithPhoneNumberMissing()
    {
        $recipient = new Recipient('1234');
        $this->assertEquals('1234', $recipient->getId());
    }

    public function testGettersWithIdMissing()
    {
        $recipient = new Recipient(null, '31612345678');
        $this->assertEquals('31612345678', $recipient->getPhoneNumber());
    }

    public function testFormatAsArrayWithId()
    {
        $recipient = new Recipient('1234');
        $this->assertEquals([
            'id' => '1234',
        ], $recipient->formatAsArray());
    }

    public function testFormatAsArrayWithPhoneNumber()
    {
        $recipient = new Recipient(null, '31612345678');
        $this->assertEquals([
            'phone_number' => '31612345678',
        ], $recipient->formatAsArray());
    }
}
