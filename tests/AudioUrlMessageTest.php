<?php

use MessageBird\FacebookMessenger\Objects\AudioUrlMessage;

class AudioUrlMessageTest extends PHPUnit_Framework_TestCase
{
    public function testGetters()
    {
        $message = new AudioUrlMessage('https://example.com/audio.mp3');
        $this->assertEquals('https://example.com/audio.mp3', $message->getUrl());
    }

    public function testFormatAsArray()
    {
        $message = new AudioUrlMessage('https://example.com/audio.mp3');
        $this->assertEquals([
            'attachment' => [
                'type' => 'audio',
                'payload' => [
                    'url' => 'https://example.com/audio.mp3',
                ],
            ],
        ], $message->formatAsArray());
    }
}
