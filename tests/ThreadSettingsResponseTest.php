<?php

use GuzzleHttp\Psr7\Response;
use MessageBird\FacebookMessenger\Responses\ThreadSettingsResponse;

class ThreadSettingsResponseTest extends PHPUnit_Framework_TestCase
{
    public function testMissingResult()
    {
        $this->setExpectedException(
            'MessageBird\FacebookMessenger\Exceptions\DomainException',
            'Result is required.'
        );
        $data = '{"foo":"bar"}';
        $response = new Response(200, [], $data);
        new ThreadSettingsResponse($response);
    }

    public function testGetters()
    {
        $data = '{"result":"Successfully added new_thread\'s CTAs"}';
        $response = new Response(200, [], $data);
        $threadSettingsResponse = new ThreadSettingsResponse($response);
        $this->assertEquals($data, $threadSettingsResponse->getBody());
        $this->assertEquals("Successfully added new_thread's CTAs", $threadSettingsResponse->getResult());
    }
}
