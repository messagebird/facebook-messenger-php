<?php

use MessageBird\FacebookMessenger\Objects\AudioFileMessage;

class AudioFileMessageTest extends PHPUnit_Framework_TestCase
{
    public function testGetters()
    {
        $message = new AudioFileMessage('php://temp', 'audio.mp3', 'foo/bar');
        $this->assertInstanceOf('GuzzleHttp\Psr7\Stream', $message->getStream());
        $this->assertEquals('audio.mp3', $message->getFilename());
        $this->assertEquals('foo/bar', $message->getContentType());
    }

    public function testFormatAsArray()
    {
        $message = new AudioFileMessage('php://temp', 'audio.mp3');
        $this->assertEquals([
            'attachment' => [
                'type' => 'audio',
                'payload' => [],
            ],
        ], $message->formatAsArray());
    }
}
